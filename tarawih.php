<?php
if(isset($_GET["english"])){
	include "assets/lang/english.php" ;
		}
else if(isset($_GET["indonesia"])){
	include "assets/lang/indonesia.php" ;
	}
else{
	include "assets/lang/indonesia.php";
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="shortcut icon" type="image/png" href="assets/icons/favicon.png"/>
    <title>Qurban Aqiqah Muslimapp | All Muslim Needs In One App</title>
    <meta name="description" content="Aplikasi waktu sholat, Membaca Quran, Navigasi Arah Kiblat, Aqiqah Online dan Qurban ">
	<meta name="keywords" content="Aplikasi Muslim App, Waktu Sholat, Membaca Quran, Navigasi Arah Kiblat, Aqiqah Online, Qurban Online">
	<meta name="robots" content="index,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/styless.css" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115158415-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115158415-1');
</script>
<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">window.dojoRequire(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us19.list-manage.com","uuid":"dd5b5b4fbe559b0d8576baa2b","lid":"a46458ad16","uniqueMethods":true}) })</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WL22FT6');</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '408538096676512');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=408538096676512&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Trustmark idEA Start -->
<!-- <script type="text/javascript" src="https://www.idea.or.id/assets/js/trustmark_badge.js"></script><link rel="stylesheet" href="https://www.idea.or.id/assets/css/trustmark_badge.css" /><input type="hidden" id="hid_trustmark_code" value="HCONLnpBGtqdSIVYse81aAWX"/><input type="hidden" id="hid_base_url" value="https://www.idea.or.id/" /><input type="hidden" id="hid_alt" value="no"/><div id="idEA_trustmark"><div id="idEA_trustmark_wrapper"><img id="idEA_trustmark_image" width="105px" /><div id="idEA_trustmark_verified">Verifikasi Hingga</div><div id="idEA_trustmark_until"></div></div></div> -->
<!-- Trustmark idEA End -->
</head>
<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WL22FT6"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                      <a href="https://qurban.muslimapp.id" class="nav-link">Qurban<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://aqiqah.muslimapp.id" class="nav-link">Aqiqah<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://muslimapp.id/Tracking/" class="nav-link"><?php echo $nav_order; ?><span class="sr-only">(current)</span></a>
                  </li>
                  <!-- <li class="nav-item">
                      <a href="https://partner.muslimapp.id" class="nav-link"><?php echo $nav_mitra; ?><span class="sr-only">(current)</span></a>
                  </li> -->
                  <li class="nav-item">
                      <a href="https://news.muslimapp.id/" class="nav-link"><?php echo $nav_news; ?><span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a href="?english" class="nav-link">EN<!--img src="assets/icons/eng.png" class="icon-indo"--><span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="?indonesia" class="nav-link">ID<!--img src="assets/icons/indo.png" class="icon-indo"--><span class="sr-only">(current)</span></a>
                  </li>
                </ul>
            </div>
        </nav>

       

        <div class="aqiqahtothenews col-lg-12">

        <div class="our-features row">
                <div class="of-title">
                    <h1>&nbsp;</h1>
                </div>
                <div class="of-content col-lg-11 col-md-11" style="text-align:center">
                    <br/>
                    <img src="assets/images/tarawih.jpeg" style="width:100%">
                </div>
            </div>


            <div class="contact-us col-lg-12" style="margin-top:-100px">
                <div class="cu-get-now col-lg-12 hideme">
                    <h3 class="col-lg-6"><?php echo $gt; ?></h3>
                    <div class="get-now col-lg-5 col-md-6 col-sm-8">
                        <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp" class="plays"><img src="assets/icons/playstore.png" class="col-lg-6 playstore"></a>
                        <a href="https://itunes.apple.com/us/app/muslimapp-id-qurban-aqiqah-etc/id1347060636?mt=8" class="apps"><img src="assets/icons/appstore.png" class="col-lg-6 appstore"></a>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="col-lg-3 footer-logo col-md-3 col-sm-5 col-6">
                <a href="https://muslimapp.id">
                <img src="assets/icons/muslimaoo.png" class="col-lg-9">
                </a>
                <br/>
                <div id="idEA_trustmark" style="background-color:none">
                    <div> <!-- id="idEA_trustmark_wrapper" -->
                    <img id="idEA_trustmark_image" width="105px" />
                    </div>
                </div>
            </div>
            <div class="footer-contact col-lg-12 col-md-10">
                <h6 class="col-lg-12">info@muslimapp.id</h6>
                <div class="socialmedia col-lg-3">
                    <a href="https://www.facebook.com/muslimapp.id">
                        <img src="assets/icons/facebook.png">
                    </a>
                    <a href="https://www.twitter.com/muslimapp_id">
                        <img src="assets/icons/twitter.png">
                    </a>
                    <a href="https://www.instagram.com/muslimapp.id/">
                        <img src="assets/icons/ig.png">
                    </a>
                    <a href="https://www.youtube.com/channel/UCgrtp33wmhwS1Ci1UeJx8WA">
                        <img src="assets/icons/youtube.png">
                    </a>
                    <a href="https://www.linkedin.com/company/muslimapp/">
                        <img src="assets/icons/linkedin.png">
                    </a>
                </div>
                <div class="footer-menu col-lg-7">
                <a href="syaratdanketentuan.php"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot1; ?></h6></a>
                    <a href="kebijakanprivasi.php"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot2; ?></h6></a>
                    <a href="https://api.whatsapp.com/send?phone=628112333724"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot3; ?></h6></a>
                    <a href="agreeaqiqah.php"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Aqiqah</h6></a>
                    <a href="agreequrban.php"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Qurban</h6></a>
                    <a href="agreeislamicproduct.php"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Islamic Product</h6></a>
                </div>
                <div class="col-lg-12 footer-copyright">
                    <p><?php echo $cp; ?></p>
                </div>
            </div>
        </footer>

        <div class="btn-drift col-lg-12">
            <div class="float-button-wrapper">
                <div class="float-button-page">
                    <a href="https://api.whatsapp.com/send?phone=628112333724">
                        <img src='assets/icons/whatsapp@2x.png'>
                        <h6><?php echo $con; ?></h6>
                    </a>
                </div>
            </div>
        </div>

    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Trustmark idEA Start -->
    <script type="text/javascript" src="https://www.idea.or.id/assets/js/trustmark_badge.js"></script>
    <link rel="stylesheet" href="https://www.idea.or.id/assets/css/trustmark_badge.css" />
    <input type="hidden" id="hid_trustmark_code" value="HCONLnpBGtqdSIVYse81aAWX"/>
    <input type="hidden" id="hid_base_url" value="https://www.idea.or.id/" /><input type="hidden" id="hid_alt" value="no"/>
    <div style="display:none">
    <div id="idEA_trustmark_verified">Verifikasi Hingga</div><div id="idEA_trustmark_until"></div>
    </div>
    <!-- Trustmark idEA End -->

    <script>
        $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            &&
            location.hostname == this.hostname
            ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                scrollTop: target.offset().top
                }, 1000, function() {
                // Callback after animation
                // Must change focus!
                var $target = $(target);
                $target.focus();
                if ($target.is(":focus")) { // Checking if the target was focused
                    return false;
                } else {
                    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                    $target.focus(); // Set focus again
                };
                });
            }
            }
        });

        $(document).ready(function() {

            /* Every time the window is scrolled ... */
            $(window).scroll( function(){

                /* Check the location of each desired element */
                $('.hideme').each( function(i){

                    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                    var bottom_of_window = $(window).scrollTop() + $(window).height();

                    /* If the object is completely visible in the window, fade it it */
                    if( bottom_of_window > bottom_of_object ){

                        $(this).animate({'opacity':'1'},500);

                    }

                });

            });

        });

        $(window).scroll(function() {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });

        $('#return-to-top').click(function() {      // When arrow is clicked
            $('body,html').animate({
                scrollTop : 0                       // Scroll to top of body
            }, 500);
        });
    </script>

</body>
</html>