<?php
if(isset($_GET["english"])){
	include "../assets/lang/english.php" ;
		}
else if(isset($_GET["indonesia"])){
	include "../assets/lang/indonesia.php" ;
	}
else{
	include "../assets/lang/indonesia.php";
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="shortcut icon" type="image/png" href="../assets/icons/favicon.png"/>
    <title>Qurban Aqiqah Muslimapp | All Muslim Needs In One App</title>
    <meta name="description" content="Aplikasi waktu sholat, Membaca Quran, Navigasi Arah Kiblat, Aqiqah Online dan Qurban ">
	<meta name="keywords" content="Aplikasi Muslim App, Waktu Sholat, Membaca Quran, Navigasi Arah Kiblat, Aqiqah Online, Qurban Online">
	<meta name="robots" content="index,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../assets/css/styless.css" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115158415-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115158415-1');
</script>
<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">window.dojoRequire(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us19.list-manage.com","uuid":"dd5b5b4fbe559b0d8576baa2b","lid":"a46458ad16","uniqueMethods":true}) })</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WL22FT6');</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '408538096676512');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=408538096676512&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Trustmark idEA Start -->
<!-- <script type="text/javascript" src="https://www.idea.or.id/../assets/js/trustmark_badge.js"></script><link rel="stylesheet" href="https://www.idea.or.id/../assets/css/trustmark_badge.css" /><input type="hidden" id="hid_trustmark_code" value="HCONLnpBGtqdSIVYse81aAWX"/><input type="hidden" id="hid_base_url" value="https://www.idea.or.id/" /><input type="hidden" id="hid_alt" value="no"/><div id="idEA_trustmark"><div id="idEA_trustmark_wrapper"><img id="idEA_trustmark_image" width="105px" /><div id="idEA_trustmark_verified">Verifikasi Hingga</div><div id="idEA_trustmark_until"></div></div></div> -->
<!-- Trustmark idEA End -->
</head>
<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WL22FT6"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a href="https://muslimapp.id" class="nav-link">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a href="https://muslimapp.id/aplikasi" class="nav-link">Aplikasi <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://qurban.muslimapp.id" class="nav-link">Qurban<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://aqiqah.muslimapp.id" class="nav-link">Aqiqah<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://muslimapp.id/Tracking/" class="nav-link"><?php echo $nav_order; ?><span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://news.muslimapp.id/" class="nav-link"><?php echo $nav_news; ?><span class="sr-only">(current)</span></a>
                  </li>
                </ul>
            </div>
        </nav>

       

        <div class="lp-features col-lg-12">
            <div class="lp-features-title col-lg-5 col-md-5 col-sm-5 col-7 hideme" style="margin-top:30px">
                <h3 class="lp-features-h1" style="text-align:left">
                    <a href="https://muslimapp.id" style="color:#0b4624">Muslimapp.id</a>
                </h3>
                <h5 class="lp-features-h2" style="text-align:left">
                Aplikasi ini dikembangkan dan dipublikasikan oleh 
                PT. Digital Muslim Global untuk dan atas nama 
                Muslimapp.id, diberikan kepada Anda sebagai pengguna
                berdasarkan syarat dan ketentuan penggunaan berikut 
                (syarat dan ketentuan). <br/><br/>
                Pengguna disarankan membaca dengan seksama karena dapat
                berdampak kepada hak dan kewajiban pengguna berdasarkan
                hukum. <br/><br/>
                Dengan mendaftar dan/atau menggunakan aplikasi
                Muslimapp.id, maka pengguna dianggap telah membaca,
                mengerti, memahami, menunjukkan/memberikan persetujuan
                terkait dengan keseluruhan syarat dan ketentuan ini. 
                <br/><br/>
                Apabila pengguna tidak menerima sebagian atau keseluruhan dari
                syarat dan ketentuan ini, maka pengguna harus segera
                menghentikan semua penggunaan aplikasi Muslimapp.id dan
                tidak diperkenankan menggunakan aplikasi muslimapp.id.
                <br/><br/>
                Aplikasi ini adalah aplikasi informasi dan kegunaan/manfaat
                pendukung sarana ibadah yang terkait dengan agama Islam.
                Muslimapp.id dan DMG tidak berafiliasi dan tidak mendukung
                suatu organisasi politik tertentu, ajaran/sekte, ideologi dan
                denominasi.
                </h5>
            </div>
        </div>

        <div class="aqiqahtothenews col-lg-12" style="margin-top:-200px">

        <div class="our-features row">
                <div class="of-title hideme">
                    <h1>Syarat dan Ketentuan</h1>
                </div>
                <div class="of-content col-lg-11 col-md-11">
                    <h6>
                    1. Definisi <br/>
                    PT. Digital Muslim Global (DMG) adalah suatu perseroan terbatas yang didirikan berdasarkan hukum negara Indonesia yang menjalankan kegiatan usaha di bidang teknologi informasi dan jasa kemasyarakatan lainnya selanjutnya disebut Muslimapp.id. Dalam syarat dan Ketentuan ini, kecuali lingkupnya menentukan lain, kata berikut harus memiliki arti sebagai berikut : <br/>
a.Syarat dan Ketentuan adalah perjanjian antara Pengguna dan Muslimapp.id yang berisikan seperangkat peraturan yang mengatur hak, kewajiban, tanggung jawab Pengguna dan Muslimapp.id, serta tata cara penggunaan layanan Muslimapp.id. <br/>
b.“Pengguna” berarti pihak yang menggunakan layanan aplikasi Muslimapp.id, termasuk namun tidak terbatas pada pembeli, mitra, vendor, atau pihak lainnya dalam fitur tertentu pada aplikasi Muslimapp.id. <br/>
c.“Pembeli” adalah Pengguna terdaftar yang melakukan permintaan atas barang yang ditawarkan pada fitur dalam aplikasi Muslimapp.id.<br/>
d.“Mitra” berarti pihak pengguna berdasarkan mitra penjualan barang dalam bentuk penyediaan, penawaran atau bundling.<br/>
e.“Barang” berarti benda berwujud / memiliki fisik barang yang dapat diantar yang tersedia dan ditawarkan pada aplikasi Muslimapp.id.<br/>
f.“Aplikasi” berarti aplikasi seluler dan web Muslimapp.id.<br/>
g.“Perangkat” berarti perangkat seluler dan/atau PC pengguna menjalankan aplikasi.<br/>
h.“Layanan” berarti layanan, informasi, fungsi dan fitur yang disediakan untuk anda melalui aplikasi.<br/>
i.“Password” berarti kata sandi yang sah yang anda gunakan Bersama dengan nama identitas pengguna (user id) untuk mengakses dan menggunakan aplikasi dan/atau layanan yang dilindungi kata sandi atau pengaman.<br/>
j.“Materi Pengguna” berarti setiap informasi, teks, dan/atau materi lain yang diajukan oleh anda untuk dimasukkan dan/atau memposting melalui aplikasi dan / atau layanan.<br/>
k.“User Id” berarti identitas nama atau nama identifikasi yang mengidentifikasi anda sebagai pengguna aplikasi.<br/>
l.“Rekening” berarti rekening Bank PT. Digital Muslim Global (DMG) untuk proses transaksi pembelian melalui aplikasi Muslimapp.id.  <br/>
<br/>

2.Lisensi Penggunaan Aplikasi<br/>
Aplikasi ini adalah hak milik PT. Digital Muslim Global dan tidak boleh digunakan selain sesuai dengan ketentuan yang ditetapkan disini. Apabila pengguna menyetujui syarat dan ketentuan ini dengan mengklik pilihan tombol “SAYA SETUJU”, Muslimapp.id memberi pengguna hak pribadi, terbatas, non ekslusif, tidak dapat dialihkan, tidak dapat dilisensikan untuk menggunakan aplikasi pada suatu perangkat pribadi, untuk penggunaan pribadi dan non komersial saja dan selalu tunduk pada syarat dan ketentuan ini, untuk tujuan mengakses dan menggunakan layanan.
Muslimapp.id tidak memungut biaya apapun terkait dengan registrasi atau pendaftaran pada pengguna.
Muslimapp.id memiliki semua hak yang tidak diberikan disini.
Pengguna tidak diperbolehkan dalam keadaan apapun : menyalin, menjual, mendistribusikan, mentransmisikan, menampilkan secara publik, menyewakan, mengalihkan, menerbitkan atau mereproduksi aplikasi atau bagian apapun darinya dalam bentuk apapun dengan cara apapun, dan/atau menyesuaikan, memodifikasi, mendekompilasi, membongkar dan/atau merekayasa balik aplikasi atau bagian apapun darinya.
Setiap pelanggaran persyaratan atau pembatasan dalam syarat dan ketentuan ini akan mengakibatkan penghentian segera dan otomatis semua hak dan lisensi yang diberikan disini kepada pengguna. 
<br/><br/>

3.Penggunaan Layanan <br/>
Pengguna harus mematuhi semua hukum dan peraturan yang berlaku dalam penggunaan anda atas aplikasi dan layanan, tidak sebagai orang atau entitas apapun atau secara salah menyatakan atau salah merepresentasikan afiliasi anda dengan orang atau entitas apapun, tidak mengirim, mendistribusikan, atau mengunggah, dengan cara apapun, data atau materi yang mengandung virus, kode berbahaya, atau komponen berbahaya yang dapat merusak atau merusak pengoperasian komputer atau perangkat orang lain, dan tidak memposting, mempromosikan atau mengirimkan melalui aplikasi dan/atau layanan materi yang melanggar hukum, kesusilaan, melecehkan, memfitnah, berbahaya, vulgar, tidak senonoh atau tidak pantas dalam bentuk apapun.
Pengguna menerapkan asas “utmost good faith” (itikad baik) dalam penggunaan aplikasi dan memberikan informasi data identitas pribadi yang diperlukan dalam rangka pengisian data pribadi untuk keperluan pendaftaran/registrasi pada aplikasi Muslimapp.id.
Muslimapp.id tanpa pemberitahuan dahulu kepada pengguna, memiliki kewenangan untuk melakukan tindakan yang perlu atas setiap dugaan pelanggaran atau pelanggaran syarat dan ketentuan dan/atau hukum yang berlaku, yakni tindakan berupa penghapusan akun pengguna.
Pengguna bertanggung jawab secara pribadi untuk menjaga kerahasiaan akun dan password dan untuk semua aktivitas yang terjadi dalam akun pengguna.
Muslimapp.id tidak akan meminta user id, password, informasi rekening bank pribadi milik akun pengguna untuk alasan apapun, oleh karena itu Muslimapp.id menghimbau kepada pengguna agar tidak memberikan data tersebut maupun data penting lainnya kepada pihak yang mengatasnamakan Muslimapp.id atau pihak lain yang tidak dapat dijamin keamanannya.
Pengguna dengan ini menyatakan bahwa Muslimapp.id tidak bertanggung jawab atas kerugian ataupun kendala yang timbul atas penyalahgunaan akun pengguna yang diakibatkan oleh kelalaian pengguna, termasuk namun tidak terbatas pada meminjamkan atau memberikan akses akun kepada pihak lain, mengakses link atau tautan yang diberikan oleh pihak lain, memberikan atau memperlihatkan kode verifikasi (OTP), password atau email kepada pihak lain, maupun kelalaian pengguna lainnya yang mengakibatkan kerugian ataupun kendala pada akun pengguna. 
Pengguna memahami dan menyetujui bahwa untuk mempergunakan fasilitas keamanan one time password (OTP) maka penyedia jasa telekomunikasi terkait dapat sewaktu-waktu   mengenakan biaya kepada pengguna.
Muslimapp.id dapat dari waktu ke waktu tanpa memberikan alasan atau pemberitahuan sebelumnya, meningkatkan, memodifikasi, mengubah, menangguhkan, menghentikan penyediaan atau menghapus, baik secara keseluruhan atau sebagian, aplikasi dan/atau layanan dan tidak akan bertanggung jawab jika setiap peningkatan, modifikasi, penangguhan, atau perubahan semacam itu mencegah anda mengakses aplikasi dan/atau layanan atau bagian apapun darinya. Informasi dan/atau materi lain yang disediakan melalui aplikasi atau layanan dapat dimodifikasi, dihapus atau diganti dari waktu ke waktu dan kapan saja dalam kebijaksanaan mutlak Muslimapp.id.
Muslimapp.id berhak tetapi tidak berkewajiban untuk memantau, menyaring atau mengendalikan aktivitas, konten, atau materi apapun yang disediakan melalui aplikasi atau layanan. Muslimapp.id dapat menyelidiki setiap pelanggaran terhadap syarat dan ketentuan ini dan dapat mengambil tindakan yang dianggap tepat; menghapus, memblokir, menolak atau merelokasi materi pengguna yang tersedia melalui aplikasi atau layanan; untuk mencegah atau membatasi akses setiap pengguna ke aplikasi atau layanan; dan/atau melaporkan kegiatan apapun yang diduga melanggar hukum, undang-undang atau peraturan yang berlaku kepada pihak berwenang yang sesuai dan bekerjasama dengan pihak berwenang.
<br/><br/>

4.Ketentuan Layanan baru<br/>
Selain syarat dan ketentuan ini, penggunaan aspek tertentu dari aplikasi atau layanan dan/atau versi aplikasi atau layanan yang lebih komprehensif atau diperbarui dapat dikenakan syarat dan ketentuan tambahan (“ketentuan tambahan”), yang akan berlaku dengan ketentuan penuh. Dengan menyetujui syarat dan ketentuan ini, pengguna juga menyetujui ketentuan tambahan tersebut.
Muslimapp.id berhak (tetapi tidak berkewajiban) untuk memperkenalkan produk, aplikasi, program, layanan, fungsi, dan/atau fitur baru (secara kolektif, “layanan baru”) ke aplikasi dan/atau layanan. Istilah “Layanan” termasuk Layanan Baru yang diberikan tanpa biaya atau dengan biaya kecuali jika dinyatakan lain.
Semua Layanan Baru akan diatur oleh syarat dan ketentuan ini dan dapat tunduk pada ketentuan tambahan yang harus pengguna setujui sebelum akses dan penggunaan Layanan Baru tersebut diberikan. Apabila ada ketidakkonsistenan antara syarat dan ketentuan ini dan Ketentuan Tambahan, Ketentuan Tambahan akan berlaku sejauh ketidaksesuaian tersebut terkait dengan layanan, produk dan/atau program yang bersangkutan kecuali apabila disediakan sebaliknya.
Pengguna mungkin diharuskan untuk melakukan pembayaran tambahan untuk menggunakan atau mengakses layanan baru.
Google Maps : Beberapa bagian dari aplikasi dan layanan mengimplementasikan Google Maps. Dengan mengakses dan menggunakan bagian aplikasi dan/atau layanan yang menerapkan Google Maps, pengguna juga setuju untuk terikat dengan ketentuan layanan Google dan kebijakan privasi Google, yang dapat diperbarui dari waktu ke waktu.
<br/><br/>

5.Penolakan <br/>
Data dan informasi yang disediakan melalui aplikasi dan/atau layanan mungkin berasal dari berbagai sumber, termasuk penyedia konten pihak ketiga. Data dan/atau informasi tersebut disediakan hanya untuk tujuan informasi dan tidak dimaksudkan untuk menggantikan informasi resmi yang disediakan oleh masjid setempat anda atau otoritas keagamaan lainnya.
Sementara setiap upaya dilakukan untuk memastikan standar dan kualitas yang tinggi untuk data dan informasi yang diberikan melalui aplikasi dan/atau layanan, DMG dan Muslimapp.id tidak membuat pernyataan mengenai keakuratan, ketepatan waktu, kecukupan atau nilai komersil dari semua data tersebut dan/atau informasi. DMG dan Muslimapp.id salah satu penyedia konten tidak bertanggungjawab atas ketidakakuratan, kesalahan atau keterlambatan dalam data dan informasi yang diberikan melalui aplikasi dan/atau layanan, atau untuk tindakan apapun yang diambil dengan mengandalkannya. Pengguna tidak boleh bertindak atas data atau informasi semacam itu tanpa terlebih dahulu secara independent memverifikasi isinya.
Muslimapp.id dan DMG tidak berkewajiban untuk memantau atau meninjau pesan, posting, transmisi, dan sejenisnya pada atau dapat diakses melalui aplikasi dan/atau layanan, dan tidak bertanggungjawab atau kewajiban yang timbul dari konten pada atau dapat diakses melalui aplikasi dan/atau layanan atau untuk kesalahan, pencemaran nama baik, fitnah, kelalaian, kebohongan, kesusilaan, pornografi, tidak senonoh, ketidakakuratan atau materi atau konten yang tidak pantas lainnya pada atau dapat diakses melalui aplikasi dan/atau layanan.
Setiap tautan ke situs web atau halaman web lain bukan merupakan pengesahan atau verifikasi situs web atau halaman web tersebut dan hanya boleh diakses dengan resiko pengguna sendiri.
<br/><br/>


6.Tidak Ada Jaminan <br/>
Aplikasi dan layanan semua informasi, materi, layanan dan fungsi yang terdapat didalamnya termasuk perangkat lunak, program, data, database, teks, grafik, foto, animasi, audio, musik, video, tautan atau materi lainnya, disediakan “sebagaimana adanya” dan “sebagaimana tersedia”. Tidak ada jaminan apapun, tersirat, tersurat baik secara hukum, termasuk namun tidak terbatas pada jaminan title, non-pelanggaran hak pihak, dapat diperdagangkan, kualitas yang memuaskan, kesesuaian untuk tujuan tertentu dan bebas dari virus komputer atau lainnya, destruktif atau kode, agen, program, dalam konjungsi dengan aplikasi atau layanan. Tanpa mengesampingkan keumuman dari yang telah disebutkan sebelumnya, Muslimapp.id tidak menjamin : ketepatan, ketepatan waktu, kecukupan atau kelengkapan informasi, materi, layanan dan/atau fungsi yang disediakan melalui aplikasi dan/atau layanan, bahwa penggunaan dan/atau akses anda ke aplikasi, layanan atau informasi apapun atau materi apapun yang disediakan melalui aplikasi atau layanan, atau pengoperasian aplikasi, tidak akan terganggu, aman, atau bebas dari kesalahan atau kelalaian atau yang setiap cacat yang diidentifikasi akan diperbaiki, bahwa aplikasi layanan atau informasi atau materi apapun yang disediakan melalui aplikasi atau layanan akan memenuhi persyaratan anda atau bebas.
<br/><br/>

7.Pembatasan Tanggung Jawab <br/>
Muslimapp.id selalu berupaya untuk menjaga layanan Muslimapp.id aman, nyaman, dan berfungsi dengan baik, tapi kami tidak dapat menjamin operasi terus menerus atau akses layanan kami dapat selalu sempurna. Informasi dan data dalam aplikasi Muslimapp.id memiliki kemungkinan tidak terjadi secara real time.
Pengguna setuju bahwa memanfaatkan layanan Muslimapp.id atas resiko penggunaan sendiri, dan layanan Muslimapp.id diberikan kepada pengguna “SEBAGAIMANA ADANYA” dan “SEBAGAIMANA TERSEDIA”
Muslimapp.id dan DMG tidak akan memiliki alasan atau alasan apapun yang bertanggungjawab, bahkan jika DMG dan/atau muslimapp.id telah diberitahu tentang kemungkinan kerusakan, kerugian atau pengeluaran tersebut, atas segala kerusakan, kerugian atau biaya, termasuk kerusakan langsung, tidak langsung, khusus, atau konsekuensial, atau kerugian ekonomi, yang timbul dari atau sehubungan dengan setiap akses, penggunaan atau ketidakmampuan untuk mengakses atau menggunakan aplikasi atau layanan; setiap akses, penggunaan atau ketidakmampuan untuk mengakses atau menggunakan aplikasi atau layanan; setiap system, server atau kegagalan koneksi, kesalahan, kelalaian, gangguan, keterlambatan transmisi, virus komputer atau kode jahat, merusak atau rusak lainnya, program agen; setiap penggunaan atau akses ke situs web atau halaman web lain yang disediakan melalui aplikasi atau layanan; setiap layanan, produk, informasi, data, perangkat lunak atau materi lain yang diperoleh atau diunduh dari aplikasi atau layanan atau dari situs web atau halaman web lain yang disediakan melalui aplikasi atau layanan atau dari pihak lain yang dirujuk oleh atau melalui penggunaan aplikasi atau layanan, atau; penggunaan atau penyalahgunaan aplikasi atau layanan pengguna. Dalam hal apapun DMG atau muslimapp.id tidak akan bertanggungjawab kepada pengguna, atau pihak lain untuk : jumlah yang jatuh tempo dari pengguna lain dari aplikasi atau layanan sehubungan dengan pembelian produk/layanan apapun; kerusakan yang timbul sehubungan dengan pengunduhan atau pemasangan, atau ketidakmampuan untuk mengunduh atau memasang aplikasi, oleh anda atau pihak ketiga lainnya, dan/atau; penjualan, pajak.
Sejauh diizinkan oleh hukum yang berlaku, Muslimapp.id (termasuk perusahaan, direktur, dan karyawan adalah tidak bertanggung jawab, atas segala kerusakan atau kerugian (termasuk namun tidak terbatas pada hilangnya uang atau kerugian tak berwujud lainnya) yang diakibatkan secara langsung atau tidak langsung dari :
Penggunaan atau ketidakmampuan pengguna dalam menggunakan layanan Muslimapp.id;
Harga, pengiriman atau petunjuk lain yang tersedia dalam layanan Muslimapp.id;
Keterlambatan atau gangguan dalam layanan Muslimapp.id;
Kelalaian dan kerugian yang ditimbulkan oleh pengguna lainnya atau pihak lainnya;
Kualitas barang/paket;
Pengiriman barang/paket;
Pelanggaran Hak atas Kekayaan Intelektual;
Pencemaran nama baik pihak lain;
Kerugian akibat pembayaran tidak resmi kepada pihak lain selain ke rekening resmi Muslimapp.id, yang dengan cara apapun mengatasnamakan Muslimapp.id ataupun kelalaian penulisan langsung dan/atau informasi lainnya dan/atau kelalaian dari pihak bank;
Virus atau perangkat lunak berbahaya lainnya yang diperoleh dengan mengakses, atau menghubungkan ke layanan Muslimapp.id;
Gangguan, kesalahan, atau ketidakakuratan apapun dalam layanan Muslimapp.id;
Kerusakan pada hardware pengguna dari penggunaan setiap layanan Muslimapp.id;
Isi, tindakan, atau tidak adanya tindakan dari pihak ketgiga, termasuk terkait dengan produk yang ada dalam aplikasi Muslimapp.id yang tidak sesuai;
Tindak penegakkan yang diambil sehubungan dengan akun pengguna;
Adanya tindakan peretasan yang dilakukan oleh pihak lainnya kepada akun pengguna.
<br/><br/>

8.Konten dan Harga<br/>
Muslimapp.id menyediakan fitur pembelian dalam mendukung pelaksanaan ibadah pengguna berupa kebutuhan atas qurban, aqiqah, GPS Tracking Haji dan Umroh, serta Islamic product. 
Harga Paket / barang yang terdapat dalam aplikasi Muslimapp.id adalah harga yang ditetapkan oleh Muslimapp.id.
Muslimapp.id mencoba untuk menawarkan informasi secara benar dan sesuai, Muslimapp.id tidak bisa menjanjikan bahwa katalog akan selalu akurat dan terkini, dan pengguna setuju bahwa pengguna tidak akan meminta Muslimapp.id bertanggung jawab atas ketimpangan dalam katalog-katalog mungkin termasuk hak cipta, merek dagang atau hak milik lainnya.
Pengguna/pembeli memahami dan menyetujui bahwa kesalahan keterangan harga dan informasi lainnya yang disebabkan tidak terbaruinya halaman situs Muslimapp.id dikarenakan pengguna/pembeli tidak mengupdate pembaruan aplikasi Muslimapp.id, sehingga tanggung jawab berkaitan dengan ini adalah tanggung jawab pengguna/pembeli, dan Pengguna memahami dan menyetujui bahwa setiap masalah dan/atau perselisihan yang terjadi akibat kesalahpahaman tentang harga bukanlah merupakan tanggung jawab Muslimapp.id.
Dengan melakukan pemesanan paket/barang melalui Muslimapp.id, pengguna menyetujui untuk membayar total biaya yang harus dibayarkan sebagaimana tertera dalam invoice pembayaran, yang terdiri dari harga barang, ongkos kirim dan biaya-biaya lain yang mungkin timbul dan akan diuraikan secara terinci dalam invoice pembayaran. Pengguna setuju untuk melakukan pembayaran melalui metode pembayaran yang telah dipilih sebelumnya oleh pengguna.
Muslimapp.id untuk saat ini hanya melayani transaksi pembelian paket/barang dalam mata uang Rupiah Indonesia (Rp), dan Muslimapp.id tidak menetapkan Batasan harga maksimal pembelian paket/barang.
<br/><br/>

9.Transaksi Pembelian <br/>
Pembeli wajib bertransaksi melalui prosedur transaksi yang telah ditetapkan oleh Muslimapp.id. pembeli melakukan pembayaran dengan menggunakan metode pembayaran yang sebelumnya telah dipilih oleh pembeli.
Saat melakukan pembelian paket/barang, pembeli menyetujui bahwa :
Pembeli bertanggung jawab untuk membaca, memahami, dan menyetujui informasi/deskripsi keseluruhan barang sebelum membuat tawaran atau komitmen untuk membeli.
Pengguna masuk ke dalam kontrak yang mengikat secara hokum untuk membeli barang ketika pengguna membeli suatu barang.
Pembeli memahami dan menyetujui bahwa ketersediaan stok barang merupakan sesuatu yang dimungkinkan dapat berubah sewaktu-waktu. Sehingga dalam keadaan stok barang kosong, maka Muslimapp.id akan menolak order, dan pembayaran atas barang yang bersangkutan dikembalikan kepada pembeli.
Pembeli memahami sepenuhnya dan menyetujui bahwa segala transaksi yang diakukan selain melalui rekening resmi Muslimapp.id adalah merupakan tanggung jawab pribadi dari pembeli.
Muslimapp.id memiliki kewenangan sepenuhnya untuk menolak pembayaran tanpa pemberitahuan terlebih dahulu.
Pembayaran oleh pembeli wajib dilakukan segera (selambat-lambatnya dalam batas waktu 9 (Sembilan) jam, setelah pembeli melakukan submit order. Jika dalam batas waktu tersebut pembayaran atau konfirmasi pembayaran belum dilakukan oleh pembeli, Muslimapp.id memiliki kewenangan untuk membatalkan transaksi dimaksud. Pengguna tidak berhak mengajukan klaim atau tuntutan atas pembatalan transaksi tersebut. 
Konfirmasi pembayaran dengan setoran tunai wajib disertai dengan berita pada slip setoran berupa nomor invoice dan nama. Konfirmasi pembayaran dengan setoran tunai tanpa keterangan, Muslimapp.id akan mengalami hambatan dalam mendeteksi pengirim setoran tersebut. Sehingga setoran tersebut oleh Muslimapp.id tidak akan diproses.
Pembeli menyetujui untuk tidak memberitahukan atau menyerahkan bukti pembayaran dan/atau data pembayaran kepada pihak lain selain Muslimapp.id. dalam hal terjadi kerugianakibat pemberitahuan atau penyerahan bukti pembayaran dan/atau data pembayaran oleh pembeli kepada pihak lain, maka hal tersebut akan menjadi tanggung jawab pembeli. 
Pembeli wajib melakukan konfirmasi barang, bahwa kualitas barang dalam keadaan baik, setelah menerima paket/barang yang dibeli. Muslimapp.id memberikan batas waktu 3 (tiga) hari pengiriman, untuk pembeli melakukan konfirmasi penerimaan barang dan komplain. Dan khusus untuk pembelian paket aqiqah dan qurban, Muslimapp.id memberikan batas waktu 1 (satu) hari/pada hari yang sama dengan waktu pengiriman. Jika dalam batas waktu tersebut tidak ada konfirmasi, klaim atau komplain dari pihak pembeli, maka dengan demikian pembeli menyatakan menerima barang dengan kualitas baik.
Pembeli memahami dan menyetujui bahwa setiap klaim yang diajukan setelah batas waktu tersebut, adalah bukan menjadi tanggung jawab Muslimapp.id. Kerugian yang timbul setelah lewat waktu tersebut menjadi tanggung jawab pembeli secara pribadi.
Pembeli memahami dan menyetujui bahwa masalah keterlambatan proses pembayaran dan biaya tambahan yang disebabkan oleh perbedaan bank yang pembeli pergunakan dengan bank rekening resmi Muslimapp.id adalah tanggung jawab pembeli secara pribadi.
Pengembalian dana dari Muslimapp.id kepada pembeli hanya dapat dilakukan jika dalam keadaan-keadaan tertentu berikut ini:
Kelebihan pembayaran dari pembeli atas harga barang;
Muslimapp.id tidak dapat menyanggupi pesanan karena kehabisan stok, perubahan ongkos kirim, maupun penyebab lainnya;
Setelah waktu yang ditentukan ternyata pembeli tidak menerima paket/barang pesanan.
 Apabila terjadi proses pengembalian dana, maka pengembalian akan dilakukan melalui rekening bank milik pengguna/pembeli sesuai dengan jumlah pengembalian dana.
Muslimapp.id berwenang mengambil keputusan atas permasalahan-permasalahan transaksi yang belum terselesaikan dengan melihat bukti-bukti yang ada. Keputusan Muslimapp.id adalah keputusan akhir yang tidak dapat diganggu gugat dan mengikat pembeli untuk mematuhinya.
Pembeli wajib melakukan pembayaran dengan nominal yang sesuai dengan jumlah tagihan beserta kode pembayaran (apabila ada) yang tertera pada invoice. DMG dan Muslimapp.id tidak bertanggung jawab atas kerugian yang dialami pembeli apabila melakukan pembayaran yang tidak sesuai jumlah tagihan yang tertera pada invoice.
Pembeli memahami sepenuhnya dan menyetujui bahwa invoice yang diterbitkan adalah atas nama pembeli.
Pengiriman barang adalah bebas biaya pengiriman untuk dalam kota/wilayah yang menjadi cakupan wilayah kerja Muslimapp.id (hanya untuk paket aqiqah dan qurban/tidak berlaku untuk pembelian barang selain dari fitur tersebut). Pembeli memahami dan menyetujui bahwa biaya pengiriman akan menjadi beban dari pembeli jika domisili pembeli diluar wilayah kerja Muslimapp.id dan pembelian barang diluar produk/paket aqiqah dan qurban.
 Pembeli memahami dan mengerti bahwa Muslimapp.id telah melakukan usaha sebaik mungkin dalam memberikan informasi tarif pengiriman kepada pembeli berdasarkan domisili pembeli, namun Muslimappp.id tidak dapat menjamin keakuratan data tersebut dengan yang ada pada wilayah kerja Muslimapp.id setempat. 
 <br/><br/>

10.Promo <br/>
Muslimapp.id sewaktu-waktu dapat mengadakan kegiatan promosi (selanjutnya disebut sebagai “promo”) dengan syarat dan ketentuan yag mungkin berbeda pada masing-masing kegiatan promo. Pembeli dihimbau untuk membaca dengan seksama syarat dan ketentuan promo tersebut.
Muslimapp.id berhak, tanpa pemberitahuan sebelumnya melakukan tindakan-tndakan yang diperlukan termasuk namun tidak terbatas pada menarik subsidi atau cashback, mencabut promo, membatalkan transaksi, menahan dana, serta hal-hal lainnya jika ditemukan adanya manipulasi, pelanggaran maupun pemanfaatan promo untuk kepentingan pribadi pengguna atau pihak lain, maupun indikasi kecurangan atau pelanggaran syarat dan ketentuan Muslimapp.id dan ketentuan hokum yang berlaku di wilayah negara Indonesia.
<br/><br/>
11.Ganti Rugi<br/>
Pengguna akan mengganti kerugian dan membebaskan DMG dan Muslimapp.id dari dan terhadap setiap dan semua klaim, tindakan, proses, gugatan, kewajiban, kerugian, kerusakan, penyelesaian, denda, biaya atau pengeluaran (termasuk biaya dan pengeluaran pengacara dan klien (legal atau sebaliknya), yang DMG atau Muslimapp.id dapat mempertahankan atau dikenakan, langsung atau tidak langsung, dengan alasan DMG telah menyediakan aplikasi dan/atau layanan kepada pengguna atau telah masuk ke dalam syarat dan ketentuan ini dengan pengguna atau pemberlakuan DMG dan/atau hak-hak Muslimapp.id di bawah syarat dan ketentuan ini atau dalam bertindak atas instruksi apapun yang mungkin pengguna berikan sehubungan dengan aplikasi atau layanan atau kelalaian, penipuan dan/atau kesalahan apapun pada bagian pengguna atau pelanggaran pengguna terhadap syarat dan ketentuan ini.
Pengguna akan melepaskan Muslimapp.id dari tuntutan ganti rugi dan menjaga Muslimapp.id (termasuk perusahaan, direktur, dan karyawan) dari setiap klaim atau tuntutan, termasuk biaya hukum yang wajar, yang dilakukan oleh pihak ketiga yang timbul dalam hal pengguna melanggar perjanjian, penggunaan layanan Muslimapp.id yang tidak semestinya dan/atau pelanggaran anda terhadap hukum atau hak-hak pihak ketiga.
Pengguna akan bekerjasama sepenuhnya dalam membela setiap tuduhan atau proses hukum pihak ketiga. DMG berhak untuk menganggap control dan pembelaan ekslusif dari segala sesuatu yang dilindungi dari kerugian berdasarkan klausul 11 ini.
<br/><br/>
12.Kekayaan Intelektual<br/>
Semua hak cipta dan kekayaan intelektual lainnya dan hak kepemilikan dalam konten, termasuk namun tidak terbatas pada teks, perangkat lunak, kode, skrip, halaman web, music, suara, foto, video, grafik, antarmuka pengguna grafis, bentuk, diagram atau materi lain yang terkandung di aplikasi atau layanan (konten), milik Muslimapp.id atau pemberi lisensinya kecuali dinyatakan lain.
Pengguna dapat mengakses materi yang ditampilkan di aplikasi atau melalui layanan untuk penggunaan non komersial pengguna asalkan pengguna juga memegang semua hak cipta dan pemberitahuan hak milik lainnya yang terkandung pada materi. pengguna tidak boleh, bagaimanapun, menyalin, mereproduksi, mendistribusikan, memodifikasi, mengirim, menggunakan kembali, memposting ulang, atau menggunakan konten untuk tujuan publik atau komersial tanpa izin tertulis sebelumnya dari DMG. Merk dagang, logo, dan merk layanan (secara kolektif disebut “merk dagang”) yang ditampilkan di aplikasi adalah merk dagang terdaftar atau tidak terdaftar dari DMG atau muslimapp.id atau dimana berlaku, pemilik pihak ketiga lainnya. Tidak ada hak atau lisensi yang diberikan kepada pihak manapun yang mengakses aplikasi atau layanan untuk mengunduh, memperbanyak atau menggunakan merk dagang semacam itu.
Harap baca kebijakan pemberitahuan dan pengambilan DMG dibawah ini untuk perincian tentang bagaimana DMG memperlakukan pemberitahuan pelanggaran hak cipta potensial atau hak kekayaan intelektual lainnya. 
<br/><br/>

13.System Pihak Ketiga<br/>
Bagian layanan hanya dapat diakses dan/atau digunakan saat masuk ke system pihak ketiga seperti google. Setiap penggunaan atau akses ke bagian layanan tersebut dan setiap informasi, data, instruksi atau komunikasi (“komunikasi”) dapat merujuk ke akun pengguna dengan system pihak ketiga tersebut (apakah akses, penggunaan, atau komunikasi tersebut diizinkan oleh pengguna atau tidak) dianggap sebagai (a) penggunaan atau akses layanan oleh pengguna dan/atau, (b) komunikasi yang dikirimkan dan diterbitkan secara sah oleh pengguna. Pengguna harus terikat dengan akses, penggunaan, dan/atau komunikasi apapun yang dapat merujuk ke akun pengguna dengan system pihak ketiga tersebut, dan pengguna setuju bahwa DMG berhak (tetapi tidak berkewajiban) untuk bertindak, mengandalkan dan/atau membuat pengguna bertanggung jawab sepenuhnya dan bertanggung jawab dalam hal itu seolah-olah hal yang sama dilakukan atau atau dikirim oleh pengguna.
Dalam situasi apapun tidak dapat ditafsirkan bahwa, dalam hal akses pengguna ke dan penggunaan system (termasuk system perpesanan), aplikasi, layanan, konten, materi, produk, atau program dari pihak ketiga mana pun, DMG atau Muslimapp.id adalah pihak dalam setiap transaksi, jika ada, antara pengguna dan pihak ketiga atau yang didukung DMG atau Muslimapp.id, sponsor, menyertifikasi, atau terlibat dalam penyediaan layanan, produk, aplikasi, atau program yang dapat diakses melalui aplikasi atau layanan. DMG dan/atau Muslimapp.id tidak akan bertanggungjawab dengan cara apapun atas akses pengguna ke pengguna dan penggunaan system pihak ketiga (termasuk system perpesanan) atau untuk produk apapun yang diperoleh dan/atau dibeli dari atau layanan yang diberikan oleh pihak ketiga manapun yang menjadi tanggungjawab pengguna atau pihak ketiga yang relevan.
Anda mengakui dan setuju bahwa pengguna akan sepenuhnya bertangungjawab atas akses atau penggunaan system pihak ketiga (termasuk system perpesanan pihak ketiga), aplikasi, layanan, konten, materi, produk atau program melalui aplikasi atau layanan. Jika pengguna mengakses atau menggunakan system pihak ketiga (termasuk system perpesanan pihak ketiga) atau aplikasi, layanan, konten, materi, produk atau program dari pihak ketiga manapun, melalui aplikasi atau layanan, pengguna harus mematuhi persyaratan dan ketentuan yang relevan. Untuk akses atau penggunaan system pihak ketiga, aplikasi, layanan, konten, materi, produk, atau program dan bertanggungjawab untuk pendaftaran dan penggunaan nama pengguna atau kata sandi yang diperlukan untuk menghubungkannya. Secara khusus, harus mengakses dan menggunakan system pihak ketiga (termasuk system perpesanan pihak ketiga), aplikasi, layanan, konten, materi, produk atau program melalui aplikasi atau layanan dilarang (baik seluruhnya atau sebagian) oleh pihak ketiga seperti itu. Syarat dan ketentuan pihak, harap hentikan akses dan penggunaan tersebut sesegera mungkin setelah menyadari atau menerima pemberitahuan tentang larangan tersebut dari pihak ketiga yang relevan. DMG tidak dapat dan tidak tahu atau apakah itu menjamin bahwa penggunaan pengguna atas aplikasi atau layanan untuk mengakses atau menggunakan system pihak ketiga (termasuk system pesan pihak ketiga) atau aplikasi, layanan, konten, materi, produk atau program dari pihak ketiga manapun adalah diizinkan atau sesuai, sepenuhnya atau sebaliknya, dengan syarat dan ketentuan pihak ketiga yang berlaku. Dengan demikian, pengguna tidak akan memiliki hak atau klaim apapun, dan dengan ini melepaskan hak apapun yang anda miliki (jika ada), terhadap DMG sehubungan dengan pelanggaran atau kegagalan untuk mematuhi persyaratan dan ketentuan pihak ketiga terkait sehubungan dengan akses pengguna untuk dan/atau menggunakan system pihak ketiga tersebut (termasuk system perpesanan pihak ketiga, aplikasi, layanan, konten, materi, produk atau program melalui aplikasi atau layanan. Pengguna mengizinkan DMG dan agennya untuk mengakses system pihak ketiga (termasuk system perpesanan pihak ketiga), aplikasi, layanan, konten, materi, produk, atau program yang telah pengguna tetapkan, untuk mengambil konten atau informasi yang diminta oleh pengguna atau untuk memproses atau mengakses fungsionalitas atas permintaan pengguna. Pengguna dengan ini menunjuk DMG dan agennya sebagai agen pengguna untuk tujuan yang disebutkan diatas. Selain itu, setiap kali pengguna mengirimkan nama pengguna atau kata sandi untuk mengakses atau menggunakan system, aplikasi, layanan, konten, materi, produk, atau program pihak ketiga yang ditunjuk, pengguna akan dianggap telah mengizinkan DMG dan agennya untuk memproses permintaan pengguna dan gunakan informasi yang dikirimkan oleh pengguna.
<br/><br/>

14.Terminasi<br/>
DMG, atas kebijakannya sendiri dapat segera memberi pengguna pemberitahuan dalam salah satu cara yang ditentukan dalam klausul 16 di bawah ini, mengakhiri hak pengguna untuk mengakses dan menggunakan aplikasi dan/atau layanan dan/atau membatalkan nama pengguna dan kata sandi pengguna dan dapat menghalangi akses ke aplikasi (atau bagian apapun daripadanya) dan/atau layanan (atau bagian apapun darinya) untuk alasan apapun, termasuk tanpa Batasan, setiap pelanggaran syarat dan ketentuan ini.
Setelah penghentian persyaratan dan ketentuan ini karena alasan apapun, semua hak dan/atau lisensi yang diberikan kepada pengguna berdasarkan syarat dan ketentuan ini akan segera berhenti dan berakhir dan pengguna harus segera menghentikan akses dan penggunaan aplikasi dan layanan dengan cara apapun.
Pengakhiran syarat dan ketentuan ini dengan alasan apapun tidak akan mempengaruhi kewajiban pengguna untuk melakukan pembayaran penuh atas biaya apapun yang harus di bayar jika biaya tersebut belum dibayarkan.
<br/><br/>

15.Perubahan Terhadap Persyaratan Penggunaan<br/>
DMG dapat memberlakukan persyaratan dan ketentuan lebih lanjut dan membuat revisi/perubahan terhadap syarat dan ketentuan ini karena DMG dapat menurut kebijakannya sesuai dari waktu ke waktu (termasuk persyaratan atau perubahan yang memungkinkan DMG untuk mengenakan biaya atau merevisi biaya untuk penggunaan aplikasi dan/atau layanan). DMG akan memberi tahu pengguna tentang revisi tersebut dengan memposting revisi di aplikasi atau metode pemberitahuan lainnya yang mungkin ditetapkan oleh DMG (seperti melalui email atau bentuk komunikasi elektronik lainnya), yang pengguna setujui sebagai pemberitahuan yang cukup untuk tujuan klausul ini. Apabila pengguna tidak setuju untuk terikat oleh revisi/perubahan, pengguna harus segera menghentikan semua akses dan/atau penggunaan aplikasi dan layanan. Pengguna selanjutnya setuju bahwa jika pengguna terus menggunakan dan/atau mengakses aplikasi dan/atau layanan setelah diberitahu tentang revisi terhadap syarat dan ketentuan ini, penggunaan dan/atau akses tersebut merupakan penegasan : pengakuan oleh pengguna tentang ini syarat dan ketentuan dan perubahannnya, dan perjanjian oleh pengguna untuk mematuhi dan terikat oleh syarat dan ketentuan ini dan perubahannya.
Syarat dan Ketentuan mungkin diubah dan/atau diperbaharui dari waktu ke waktu tanpa pemberitahuan sebelumnya. Muslimapp.id menyarankan agar anda membaca secara seksama dan memeriksa halaman Syarat dan Ketentuan ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap dan mengakses dan menggunakan layanan Muslimapp.id, maka pengguna dianggap menyetujui perubahan-perubahan dalam Syarat dan Ketentuan.
<br/><br/>

16.Pemberitahuan<br/>
Setiap pemberitahuan atau komunikasi lain sehubungan dengan syarat dan ketentuan ini dapat diberikan secara elektronik apabila dikirim ke alamat yang kemudian diberitahukan oleh  penerima kepada pengirim. Apabila pemberitahuan atau komunikasi diberikan oleh DMG kepada anda secara elektronik, itu akan dianggap telah diterima pada saat pengiriman (dan laporan pengiriman yang diterima oleh DMG akan menjadi bukti penyampaian yang meyakinkan bahkan jika komunikasi tidak dibuka oleh pengguna) dan dimana diberikan ke DMG secara elektronik, itu akan dianggap telah diterima setelah dibuka oleh DMG.
<br/><br/>

17.Force Majeure <br/>
DMG dan Muslimapp.id tidak akan bertanggungjawab atas setiap non kinerja, kesalahan, interupsi atau keterlambatan dalam pelaksanaan kewajibannya atau dalam operasi aplikasi atau layanan, atau untuk ketidaktepatan, tidak dapat diandalkan atau ketidaksesuaian dari konten yang tersedia melalui aplikasi atau layanan jika jatuh tempo, secara keseluruhan atau sebagian, langsung atau tidak langsung pada suatu peristiwa atau kegagalan yang berada diluar kendali wajar DMG dan/atau Muslimapp.id (Termasuk kehendak Tuhan, bencana alam, epidemi, tindakan perang atau terorisme, tindakan pemerintah atau otoritas, kegagalan kekuasaan, tindakan atau default dari operator jaringan telekomunikasi atau operator atau tindakan pihak yang tidak bertanggungjawab untuk DMG dan/atau Muslimapp.id.
<br/><br/>

18.Ketentuan Umum<br/>
Pengguna tidak dapat mengalihkan hak anda dibawah syarat dan ketentuan ini tanpa persetujuan tertulis sebelumnya dari DMG. DMG dapat mengalihkan haknya berdasarkan persyaratan dan ketentuan ini kepada pihak ketiga manapun.
Syarat dan ketentuan ini akan mengikat pengguna dan DMG dan penerus DMG masing-masing dalam judul dan penugasan dan akan terus mengikat pengguna meskipun ada perubahan dalam nama DMG atau kepengurusan perseroan atau merger, konsolidasi atau penggabungan DMG dengan atau ke entitas lain (dalam hal ini syarat dan ketentuan akan mengikat anda ke entitas penerus DMG).
Apabila ada ketentuan dari syarat dan ketentuan ini dianggap tidak sah, illegal atau tidak dapat dilaksanakan (baik secara keseluruhan atau sebagian), ketentuan tersebut akan diputus dari syarat dan ketentuan ini dan ketentuan lainnya dari syarat dan ketentuan ini tidak boleh terpengaruh demikian.
Tidak ada kegagalan atau penundaan untuk menggunakan hak-hak DMG dan/atau Muslimapp.id berdasarkan persyaratan dan ketentuan ini akan beroperasi sebagai pengabaian atau kegagalan penundaan tersebut mempengaruhi hak untuk menegakkan hak-hak DMG dan/atau Muslimapp.id berdasarkan syarat dan ketentuan ini.
Apabila syarat dan ketentuan ini diterjemahkan ke dalam Bahasa lain selain Bahasa Indonesia, maka teks Bahasa Indonesia yang akan berlaku.
Syarat dan ketentuan ini dan hubungan kami diatur oleh dan ditafsirkan sesuai dengan hukum Negara Indonesia. Pengguna tunduk pada yurisdiksi Pengadilan di Indonesia.
Pengguna mengakui dan menyetujui bahwa catatan DMG dan setiap catatan tentang komunikasi, transaksi, instruksi atau operasi yang dibuat atau dilakukan, diproses atau dilakukan melalui aplikasi dan/atau layanan oleh pengguna atau orang yang mengaku sebagai anda, bertindak atas nama pengguna atau mengaku bertindak atas nama pengguna, dengan atau tanpa izin anda, atau catatan komunikasi, transaksi, instruksi, atau operasi apapun yang terkait dengan pengoperasian aplikasi dan/atau layanan dan rekaman komunikasi, transaksi, instruksi, atau operasi apapun yang dipelihara oleh DMG atau oleh orang yang relevan yang diberi wewenang oleh DMG terkait atau terhubung dengan aplikasi dan/atau layanan akan mengikat pengguna untuk semua tujuan apapun dan akan menjadi bukti konklusif, komunikasi, transaksi, instruksi, atau operasi semacam itu.
Selain dari Muslimapp.id, tidak ada orang atau badan yang bukan merupakan pihak dari syarat dan ketentuan ini memiliki hak dibawah kontrak (hak Pihak Ketiga), atau undang-undang serupa lainnya untuk menegakkan ketentuan syarat dan ketentuan ini, terlepas dari apakah orang atau entitas tersebut telah diidentifikasi berdasarkan nama sebagai jawaban atas deskripsi tertentu. Untuk menghindari keragu-raguan, ini tidak akan mempengaruhi hak-hak dari penerima yang diijinkan atau penerima ganti rugi dari syarat dan ketentuan ini.
Tanpa mengesampingkan keumuman dari klausul 16.h diatas, hak DMG untuk mengubah, atau membatalkan persyaratan dan ketentuan ini sesuai dengan syarat dan ketentuan ini dapat dilakukan tanpa persetujuan dari orang atau badan yang bukan pihak dari ketentuan ini dan kondisi.
Pengguna setuju dan mengakui bahwa syarat dan ketentuan dan layanan ini tidak termasuk penyediaan akses internet atau layanan telekomunikasi lainnya oleh DMG dan/atau Muslimapp.id. setiap akses internet atau layanan telekomunikasi (seperti konektivitas data seluler) yang diperlakukan oleh anda untuk mengakses dan menggunakan layanan akan menjadi tanggungjawab anda sepenuhnya dan akan diperoleh secara terpisah oleh anda, atas biaya anda sendiri, dari penyedia layanan telekomunikasi atau akses internet yang sesuai.
<br/><br/>

19.Ketentuan Lain-Lain<br/>
Apabila pengguna mempergunakan fitur-fitur yang tersedia dalam aplikasi Muslimapp.id maka pengguna dengan ini menyatakan memahami dan menyetujui segala syarat dan ketentuan yang diatur khusus sehubungan dengan fitur-fitur tersebut di bawah ini, yakni :
Penggunaan fitur simpanan tabungan qurban melalui aplikasi Muslimapp.id akan diatur lebih lanjut dalam ketentuan tersendiri.
Segala hal yang belum dan/atau tidak diatur dalam syarat dan ketentuan khusus dalam fitur tersebut maka akan sepenuhnya merujuk pada syarat dan ketentuan Muslimapp.id secara umum.
<br/><br/>


                    </h6>
                </div>
            </div>


            <div class="contact-us col-lg-12">
                <div class="cu-get-now col-lg-12 hideme">
                    <h3 class="col-lg-6"><?php echo $gt; ?></h3>
                    <div class="get-now col-lg-5 col-md-6 col-sm-8">
                        <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp" class="plays"><img src="../assets/icons/playstore.png" class="col-lg-6 playstore"></a>
                        <a href="https://itunes.apple.com/us/app/muslimapp-id-qurban-aqiqah-etc/id1347060636?mt=8" class="apps"><img src="../assets/icons/appstore.png" class="col-lg-6 appstore"></a>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="col-lg-3 footer-logo col-md-3 col-sm-5 col-6">
                <img src="../assets/icons/muslimaoo.png" class="col-lg-9"><br/>
                <div id="idEA_trustmark" style="background-color:none">
                    <div> <!-- id="idEA_trustmark_wrapper" -->
                    <img id="idEA_trustmark_image" width="105px" />
                    </div>
                </div>
            </div>
            <div class="footer-contact col-lg-12 col-md-10">
                <h6 class="col-lg-12">info@muslimapp.id</h6>
                <div class="socialmedia col-lg-3">
                    <a href="https://www.facebook.com/muslimapp.id">
                        <img src="../assets/icons/facebook.png">
                    </a>
                    <a href="https://www.twitter.com/muslimapp_id">
                        <img src="../assets/icons/twitter.png">
                    </a>
                    <a href="https://www.instagram.com/muslimapp.id/">
                        <img src="../assets/icons/ig.png">
                    </a>
                    <a href="https://www.youtube.com/channel/UCgrtp33wmhwS1Ci1UeJx8WA">
                        <img src="../assets/icons/youtube.png">
                    </a>
                    <a href="https://www.linkedin.com/company/muslimapp/">
                        <img src="../assets/icons/linkedin.png">
                    </a>
                </div>
                <div class="footer-menu col-lg-7">
                    <a href="https://muslimapp.id/syaratdanketentuan"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot1; ?></h6></a>
                    <a href="https://muslimapp.id/kebijakanprivasi"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot2; ?></h6></a>
                    <a href="https://api.whatsapp.com/send?phone=628112333724"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot3; ?></h6></a>
                    <a href="https://muslimapp.id/agreeaqiqah"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Aqiqah</h6></a>
                    <a href="https://muslimapp.id/agreequrban"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Qurban</h6></a>
                    <a href="https://muslimapp.id/agreeislamicproduct"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Islamic Product</h6></a>
                </div>
                <div class="col-lg-12 footer-copyright">
                    <p><?php echo $cp; ?></p>
                </div>
            </div>
        </footer>

        <div class="btn-drift col-lg-12">
            <div class="float-button-wrapper">
                <div class="float-button-page">
                    <a href="https://api.whatsapp.com/send?phone=628112333724">
                        <img src='../assets/icons/whatsapp@2x.png'>
                        <h6><?php echo $con; ?></h6>
                    </a>
                </div>
            </div>
        </div>

    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Trustmark idEA Start -->
    <script type="text/javascript" src="https://www.idea.or.id/../assets/js/trustmark_badge.js"></script>
    <link rel="stylesheet" href="https://www.idea.or.id/../assets/css/trustmark_badge.css" />
    <input type="hidden" id="hid_trustmark_code" value="HCONLnpBGtqdSIVYse81aAWX"/>
    <input type="hidden" id="hid_base_url" value="https://www.idea.or.id/" /><input type="hidden" id="hid_alt" value="no"/>
    <div style="display:none">
    <div id="idEA_trustmark_verified">Verifikasi Hingga</div><div id="idEA_trustmark_until"></div>
    </div>
    <!-- Trustmark idEA End -->

    <script>
        $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            &&
            location.hostname == this.hostname
            ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                scrollTop: target.offset().top
                }, 1000, function() {
                // Callback after animation
                // Must change focus!
                var $target = $(target);
                $target.focus();
                if ($target.is(":focus")) { // Checking if the target was focused
                    return false;
                } else {
                    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                    $target.focus(); // Set focus again
                };
                });
            }
            }
        });

        $(document).ready(function() {

            /* Every time the window is scrolled ... */
            $(window).scroll( function(){

                /* Check the location of each desired element */
                $('.hideme').each( function(i){

                    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                    var bottom_of_window = $(window).scrollTop() + $(window).height();

                    /* If the object is completely visible in the window, fade it it */
                    if( bottom_of_window > bottom_of_object ){

                        $(this).animate({'opacity':'1'},500);

                    }

                });

            });

        });

        $(window).scroll(function() {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });

        $('#return-to-top').click(function() {      // When arrow is clicked
            $('body,html').animate({
                scrollTop : 0                       // Scroll to top of body
            }, 500);
        });
    </script>

</body>
</html>
