<?php
if(isset($_GET["english"])){
	include "../assets/lang/english.php" ;
		}
else if(isset($_GET["indonesia"])){
	include "../assets/lang/indonesia.php" ;
	}
else{
	include "../assets/lang/indonesia.php";
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="shortcut icon" type="image/png" href="../assets/icons/favicon.png"/>
    
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Muslimapp.id" />
    <meta property="og:url" content="https://Muslimapp.id/" />
    <meta property="og:site_name" content="Muslimapp.id" />
    <meta property="og:image" content="https://muslimapp.id/../assets/icons/600px_putih.png" />
    <meta property="og:image:width" content="100" />
    <meta property="og:image:height" content="100" />

    <title>Qurban Aqiqah Muslimapp | All Muslim Needs In One App</title>
    <meta name="description" content="Aplikasi Ibadah muslimapp.id - Gratis dan Tanpa Iklan">
	<meta name="keywords" content="Muslimapp.id adalah aplikasi adzan otomatis, jadwal sholat, hingga aqiqah-qurban online. Aplikasi yang membantu ibadah menjadi lebih mudah. Gratis dan tanpa iklan.">
	<meta name="robots" content="index,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../assets/css/styless.css" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115158415-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115158415-1');
</script>
<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">window.dojoRequire(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us19.list-manage.com","uuid":"dd5b5b4fbe559b0d8576baa2b","lid":"a46458ad16","uniqueMethods":true}) })</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WL22FT6');</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '408538096676512');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=408538096676512&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Trustmark idEA Start -->
<!-- <script type="text/javascript" src="https://www.idea.or.id/../assets/js/trustmark_badge.js"></script><link rel="stylesheet" href="https://www.idea.or.id/../assets/css/trustmark_badge.css" /><input type="hidden" id="hid_trustmark_code" value="HCONLnpBGtqdSIVYse81aAWX"/><input type="hidden" id="hid_base_url" value="https://www.idea.or.id/" /><input type="hidden" id="hid_alt" value="no"/><div id="idEA_trustmark"><div id="idEA_trustmark_wrapper"><img id="idEA_trustmark_image" width="105px" /><div id="idEA_trustmark_verified">Verifikasi Hingga</div><div id="idEA_trustmark_until"></div></div></div> -->
<!-- Trustmark idEA End -->

<style>
    @media (max-width: 420px) {
        .icons-margin {
            padding:0px!important;
        }
    }
    .icons-margin-top {
        margin-top:-50px;
    }
</style>

</head>
<body>
    <div itemprop='image' itemscope='itemscope' itemtype='https://schema.org/ImageObject'>
        <meta content='../assets/icons/600px_putih.png' itemprop='url'/>
    </div> 

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WL22FT6"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <a href="https://muslimapp.id/"><img src="../assets/icons/muslimaoo.png" class="img-fluid logo-head" alt="Responsive image"></a>
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a href="https://muslimapp.id/aplikasi" class="nav-link">Aplikasi <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://qurban.muslimapp.id" class="nav-link">Qurban<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://aqiqah.muslimapp.id" class="nav-link">Aqiqah<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://muslimapp.id/Tracking/" class="nav-link"><?php echo $nav_order; ?><span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://news.muslimapp.id/" class="nav-link"><?php echo $nav_news; ?><span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= $bahasa ?></a>
                    <div class="dropdown-menu backdropdown">
                        <a href="?indonesia" class="nav-link">ID<img src="../assets/icons/indo.png" class="icon-indo"><span class="sr-only">(current)</span></a>
                        <a href="?english" class="nav-link">EN<img src="../assets/icons/eng.png" class="icon-indo"><span class="sr-only">(current)</span></a>
                    </div>
                  </li>
                </ul>
            </div>
        </nav>

        <div class="lp-features col-lg-12">
            <div class="lp-features-title col-lg-5 col-md-5 col-sm-5 col-7">
                <h1 class="lp-features-h1"><?php echo $lpfeaturesh1; ?></h1>
                <h5 class="lp-features-h2"><?php echo $lpfeaturesh2; ?></h5>
                <div class="lp-btn leftBtn col-lg-12 col-md-10 col-sm-4">
                    <div class="lp-btn-playstore col-lg-4 col-md-5">
                        <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp"><img src="../assets/icons/playstore.png" class="img-fluid btn-playstore"></a>
                    </div>
                    <div class="lp-btn-appstore col-lg-4 col-md-5">
                        <a href="https://itunes.apple.com/us/app/muslimapp-id-qurban-aqiqah-etc/id1347060636?mt=8"><img src="../assets/icons/appstore.png" class="img-fluid btn-appstore" alt="Responsive image"></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 hideme" style="text-align:center;margin-top:15px;">
                <h1 class="lp-headline-h1"><?php echo $judul; ?></h1>
                <br/>
            </div>
        </div>
        
        <div class="col-lg-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 hideme" style="text-align:center">
                <h5 class="lp-headline-h5"><?php echo $headline; ?></h5>
            </div>
        </div>

        <div class="aqiqahtothenews col-lg-12">

            <div id="adzan" class="content-qurban col-lg-12 hideme">
                <div class="qurban-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="qurban-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='../assets/icons/adzan@2x.png'>
                    </div>
                    <div class="qurban-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $adzan; ?></h2>
                        <h5><?php echo $adzandesc; ?></h5>
                        <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp" id="getApp" class="btn-link" style="width:fit-content;" onclick="ga('send', 'event', 'Call Page Download App Play Store', 'Click Page Download App Play Store', 'Melihat Halaman Donwload Aplikasi di Play Store');"><?php echo $getappnow; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 qurban-right hideme">
                    <img src='../assets/images/adzan.png'>
                </div>
            </div>

            <div id="quran" class="content-aqiqah col-lg-12">
                <div class="aqiqah-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="aqiqah-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='../assets/icons/quran@2x.png'>
                    </div>
                    <div class="aqiqah-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $quranjudul; ?></h2>
                        <h5><?php echo $qurandesc; ?>
                        </h5>
                        <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp" id="getApp" class="btn-link" style="width:fit-content;" onclick="ga('send', 'event', 'Call Page Download App Play Store', 'Click Page Download App Play Store', 'Melihat Halaman Donwload Aplikasi di Play Store');"><?php echo $getappnow; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 aqiqah-right hideme">
                    <img src='../assets/images/quran.png'>
                </div>
            </div>

            <div id="jadwal_sholat" class="content-qurban col-lg-12 hideme">
                <div class="qurban-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="qurban-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='../assets/icons/adzan@2x.png'>
                    </div>
                    <div class="qurban-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $jadwal_sholat; ?></h2>
                        <h5><?php echo $jadwal_sholatdesc; ?></h5>
                        <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp" id="getApp" class="btn-link" style="width:fit-content;" onclick="ga('send', 'event', 'Call Page Download App Play Store', 'Click Page Download App Play Store', 'Melihat Halaman Donwload Aplikasi di Play Store');"><?php echo $getappnow; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 qurban-right hideme">
                    <img src='../assets/images/jadwal_sholat.png'>
                </div>
            </div>

            <div id="arah_kiblat" class="content-aqiqah col-lg-12">
                <div class="aqiqah-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="aqiqah-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='../assets/icons/qiblat@2x.png'>
                    </div>
                    <div class="aqiqah-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $kiblat; ?></h2>
                        <h5><?php echo $kiblatdesc; ?>
                        </h5>
                        <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp" id="getApp" class="btn-link" style="width:fit-content;" onclick="ga('send', 'event', 'Call Page Download App Play Store', 'Click Page Download App Play Store', 'Melihat Halaman Donwload Aplikasi di Play Store');"><?php echo $getappnow; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 aqiqah-right hideme">
                    <img src='../assets/images/arah_kiblat.png'>
                </div>
            </div>

            <div id="pencari_masjid_terdekat" class="content-qurban col-lg-12 hideme">
                <div class="qurban-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="qurban-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='../assets/icons/masjid@2x.png'>
                    </div>
                    <div class="qurban-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $pencari_masjid_terdekat; ?></h2>
                        <h5><?php echo $pencari_masjid_terdekatdesc; ?></h5>
                        <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp" id="getApp" class="btn-link" style="width:fit-content;" onclick="ga('send', 'event', 'Call Page Download App Play Store', 'Click Page Download App Play Store', 'Melihat Halaman Donwload Aplikasi di Play Store');" ><?php echo $getappnow; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 qurban-right hideme">
                    <img src='../assets/images/pencari_masjid_terdekat.png'>
                </div>
            </div>

            <div id="hadist" class="content-aqiqah col-lg-12">
                <div class="aqiqah-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="aqiqah-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='../assets/icons/Hadist.png'>
                    </div>
                    <div class="aqiqah-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $hadist; ?></h2>
                        <h5><?php echo $hadistdesc; ?>
                        </h5>
                        <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp" id="getApp" class="btn-link" style="width:fit-content;" onclick="ga('send', 'event', 'Call Page Download App Play Store', 'Click Page Download App Play Store', 'Melihat Halaman Donwload Aplikasi di Play Store');"><?php echo $getappnow; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 aqiqah-right hideme">
                    <img src='../assets/images/hadist.png'>
                </div>
            </div>

            <div id="doa" class="content-qurban col-lg-12 hideme">
                <div class="qurban-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="qurban-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='../assets/icons/Doa.png'>
                    </div>
                    <div class="qurban-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $doa; ?></h2>
                        <h5><?php echo $doadesc; ?></h5>
                        <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp" id="getApp" class="btn-link" style="width:fit-content;" onclick="ga('send', 'event', 'Call Page Download App Play Store', 'Click Page Download App Play Store', 'Melihat Halaman Donwload Aplikasi di Play Store');"><?php echo $getappnow; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 qurban-right hideme">
                    <img src='../assets/images/doa.png'>
                </div>
            </div>

            <div id="aqiqah" class="content-aqiqah col-lg-12">
                <div class="aqiqah-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="aqiqah-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='../assets/icons/aqiqah@2x.png'>
                    </div>
                    <div class="aqiqah-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $Aqiqah; ?></h2>
                        <h5><?php echo $aqiqahh5; ?>
                        </h5>
                        <a href="https://aqiqah.muslimapp.id" class="btn-link v-content-aqiqah" onclick="ga('send', 'event', 'Call Page Aqiqah', 'Click Page Aqiqah Online', 'Melihat Halaman Web Pemesanan Aqiqah');"><?php echo $getnow; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 aqiqah-right hideme">
                    <img src='../assets/images/aqiqah_illustration.png'>
                </div>
            </div>

            <div id="qurban" class="content-qurban col-lg-12 hideme">
                <div class="qurban-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="qurban-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='../assets/icons/qurban@2x.png'>
                    </div>
                    <div class="qurban-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $Qurban; ?></h2>
                        <h5><?php echo $qurbanh5; ?>
                        </h5>
                        <a href="https://qurban.muslimapp.id" class="btn-link v-content-qurban" onclick="ga('send', 'event', 'Call Page Qurban', 'Click Page Qurban Online', 'Melihat Halaman Web Pemesanan Qurban');" id="pageQurban" style="width:fit-content;"><?php echo $qurbangn; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 qurban-right hideme">
                    <img src='../assets/images/qurban_illustration.png'>
                </div>
            </div>

            <div id="islamic-product" class="content-aqiqah col-lg-12">
                <div class="aqiqah-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="aqiqah-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='../assets/icons/islamic_product.png'>
                    </div>
                    <div class="aqiqah-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $islamic_product; ?></h2>
                        <h5><?php echo $islamic_product_desc; ?></h5>
                        <a href="https://store.muslimapp.id" class="btn-link v-content-store" id="pageStore" onclick="ga('send', 'event', 'Call Page Store', 'Click Page Store', 'Melihat Halaman Web Islamic Product');" ><?php echo $getnow_islamicproduct; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 aqiqah-right hideme">
                    <img src='../assets/images/Shopping.png'>
                </div>
            </div>

            <div id="hajj-umroh" class="content-qurban col-lg-12 hideme">
                <div class="qurban-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="qurban-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='../assets/images/Artboard 1.png'>
                    </div>
                    <div class="qurban-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $hajjumroh; ?></h2>
                        <h5><?php echo $hajjumrohh5; ?>
                        </h5>
                        <a href="https://booking.muslimapp.id" class="btn-link v-content-booking" id="pageUmroh" onclick="ga('send', 'event', 'Call Page Booking Haji dan Umroh', 'Click Page Booking Haji dan Umroh', 'Melihat Halaman Web Booking Haji dan Umroh');" style="width:fit-content;">
                            <?php echo $hajjbtn; ?>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 qurban-right hideme">
                    <img src='../assets/images/Artboard 2.png'>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 hideme" style="text-align:center">
                    <h5 class="lp-headline-h5"><?php echo $morefiturdesc; ?></h5>
                    <br/>
                </div>
            </div>

        </div>

        <footer>
            <div class="col-lg-3 footer-logo col-md-3 col-sm-5 col-6">
                <img src="../assets/icons/muslimaoo.png" class="col-lg-9"><br/>
                <div id="idEA_trustmark" style="background-color:none">
                    <div> <!-- id="idEA_trustmark_wrapper" -->
                    <img id="idEA_trustmark_image" width="105px" />
                    </div>
                </div>
            </div>
            <div class="footer-contact col-lg-12 col-md-10">
                <h6 class="col-lg-12">info@muslimapp.id</h6>
                <div class="socialmedia col-lg-3">
                    <a href="https://www.facebook.com/muslimapp.id">
                        <img src="../assets/icons/facebook.png">
                    </a>
                    <a href="https://www.twitter.com/muslimapp_id">
                        <img src="../assets/icons/twitter.png">
                    </a>
                    <a href="https://www.instagram.com/muslimapp.id/">
                        <img src="../assets/icons/ig.png">
                    </a>
                    <a href="https://www.youtube.com/channel/UCgrtp33wmhwS1Ci1UeJx8WA">
                        <img src="../assets/icons/youtube.png">
                    </a>
                    <a href="https://www.linkedin.com/company/muslimapp/">
                        <img src="../assets/icons/linkedin.png">
                    </a>
                </div>
                <div class="footer-menu col-lg-7">
                    <a href="https://muslimapp.id/syaratdanketentuan"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot1; ?></h6></a>
                    <a href="https://muslimapp.id/kebijakanprivasi"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot2; ?></h6></a>
                    <a href="https://api.whatsapp.com/send?phone=628112333724"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot3; ?></h6></a>
                    <a href="https://muslimapp.id/agreeaqiqah"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Aqiqah</h6></a>
                    <a href="https://muslimapp.id/agreequrban"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Qurban</h6></a>
                    <a href="https://muslimapp.id/agreeislamicproduct"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Islamic Product</h6></a>
                </div>
                <div class="col-lg-12 footer-copyright">
                    <p><?php echo $cp; ?></p>
                </div>
            </div>
        </footer>

        <div class="btn-drift col-lg-12">
            <div class="float-button-wrapper">
                <div class="float-button-page">
                    <a href="https://api.whatsapp.com/send?phone=628112333724">
                        <img src='../assets/icons/whatsapp@2x.png'>
                        <h6><?php echo $con; ?></h6>
                    </a>
                </div>
            </div>
        </div>

    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Trustmark idEA Start -->
    <script type="text/javascript" src="https://www.idea.or.id/../assets/js/trustmark_badge.js"></script>
    <link rel="stylesheet" href="https://www.idea.or.id/../assets/css/trustmark_badge.css" />
    <input type="hidden" id="hid_trustmark_code" value="HCONLnpBGtqdSIVYse81aAWX"/>
    <input type="hidden" id="hid_base_url" value="https://www.idea.or.id/" /><input type="hidden" id="hid_alt" value="no"/>
    <div style="display:none">
    <div id="idEA_trustmark_verified">Verifikasi Hingga</div><div id="idEA_trustmark_until"></div>
    </div>
    <!-- Trustmark idEA End -->

    <script>
        $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            &&
            location.hostname == this.hostname
            ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                scrollTop: target.offset().top
                }, 1000, function() {
                // Callback after animation
                // Must change focus!
                var $target = $(target);
                $target.focus();
                if ($target.is(":focus")) { // Checking if the target was focused
                    return false;
                } else {
                    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                    $target.focus(); // Set focus again
                };
                });
            }
            }
        });

        $(document).ready(function() {

            /* Every time the window is scrolled ... */
            $(window).scroll( function(){

                /* Check the location of each desired element */
                $('.hideme').each( function(i){

                    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                    var bottom_of_window = $(window).scrollTop() + $(window).height();

                    /* If the object is completely visible in the window, fade it it */
                    if( bottom_of_window > bottom_of_object ){

                        $(this).animate({'opacity':'1'},500);

                    }

                });

            });

        });

        $(window).scroll(function() {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });

        $('#return-to-top').click(function() {      // When arrow is clicked
            $('body,html').animate({
                scrollTop : 0                       // Scroll to top of body
            }, 500);
        });

        document.getElementById('v-content-imsak').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_type: 'Melihat Menu Imsak',
            });
        }, false);
        
        document.getElementById('v-content-berita').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'Melihat Menu Berita',
            });
        }, false);

        document.getElementById('v-content-aqiqah').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'Melihat Menu Aqiqah',
            });
        }, false);

        document.getElementById('v-content-qurban').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'Melihat Menu Qurban',
            });
        }, false);

        document.getElementById('v-content-donasi').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'Melihat Menu Donasi',
            });
        }, false);

        document.getElementById('v-content-booking').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'Melihat Menu Booking Travel Hajj Umroh',
            });
        }, false);

        document.getElementById('v-content-store').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'Melihat Menu Store Muslimapp',
            });
        }, false);
        
        document.getElementById('getApp').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'Melihat Halaman Donwload Aplikasi di Play Store',
            });
        }, false);

    </script>

</body>
</html>

