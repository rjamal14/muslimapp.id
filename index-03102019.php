<?php
if(isset($_GET["english"])){
	include "assets/lang/english.php" ;
		}
else if(isset($_GET["indonesia"])){
	include "assets/lang/indonesia.php" ;
	}
else{
	include "assets/lang/indonesia.php";
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="shortcut icon" type="image/png" href="assets/icons/favicon.png"/>
    
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Muslimapp.id" />
    <meta property="og:url" content="https://Muslimapp.id/" />
    <meta property="og:site_name" content="Muslimapp.id" />
    <meta property="og:image" content="https://muslimapp.id/assets/icons/600px_putih.png" />
    <meta property="og:image:width" content="100" />
    <meta property="og:image:height" content="100" />

    <title>Qurban Aqiqah Muslimapp | All Muslim Needs In One App</title>
    <meta name="description" content="Aplikasi waktu sholat, Membaca Quran, Navigasi Arah Kiblat, Aqiqah Online dan Qurban ">
	<meta name="keywords" content="Aplikasi Muslim App, Waktu Sholat, Membaca Quran, Navigasi Arah Kiblat, Aqiqah Online, Qurban Online">
	<meta name="robots" content="index,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/styless.css" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115158415-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115158415-1');
</script>
<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">window.dojoRequire(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us19.list-manage.com","uuid":"dd5b5b4fbe559b0d8576baa2b","lid":"a46458ad16","uniqueMethods":true}) })</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WL22FT6');</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '408538096676512');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=408538096676512&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Trustmark idEA Start -->
<!-- <script type="text/javascript" src="https://www.idea.or.id/assets/js/trustmark_badge.js"></script><link rel="stylesheet" href="https://www.idea.or.id/assets/css/trustmark_badge.css" /><input type="hidden" id="hid_trustmark_code" value="HCONLnpBGtqdSIVYse81aAWX"/><input type="hidden" id="hid_base_url" value="https://www.idea.or.id/" /><input type="hidden" id="hid_alt" value="no"/><div id="idEA_trustmark"><div id="idEA_trustmark_wrapper"><img id="idEA_trustmark_image" width="105px" /><div id="idEA_trustmark_verified">Verifikasi Hingga</div><div id="idEA_trustmark_until"></div></div></div> -->
<!-- Trustmark idEA End -->

<style>
    @media (max-width: 420px) {
        .icons-margin {
            padding:0px!important;
        }
    }
    .icons-margin-top {
        margin-top:-50px;
    }
</style>

</head>
<body>
    <div itemprop='image' itemscope='itemscope' itemtype='https://schema.org/ImageObject'>
        <meta content='assets/icons/600px_putih.png' itemprop='url'/>
    </div> 

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WL22FT6"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                      <a href="https://qurban.muslimapp.id" class="nav-link">Qurban<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://aqiqah.muslimapp.id" class="nav-link">Aqiqah<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://muslimapp.id/Tracking/" class="nav-link"><?php echo $nav_order; ?><span class="sr-only">(current)</span></a>
                  </li>
                  <!-- <li class="nav-item">
                      <a href="https://partner.muslimapp.id" class="nav-link"><?php echo $nav_mitra; ?><span class="sr-only">(current)</span></a>
                  </li> -->
                  <li class="nav-item">
                      <a href="https://news.muslimapp.id/" class="nav-link"><?php echo $nav_news; ?><span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a href="?english" class="nav-link">EN<!--img src="assets/icons/eng.png" class="icon-indo"--><span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="?indonesia" class="nav-link">ID<!--img src="assets/icons/indo.png" class="icon-indo"--><span class="sr-only">(current)</span></a>
                  </li>
                  <!-- <li class="nav-item">
                      <a href="https://accounts.muslimapp.id/" class="nav-link">Login<span class="sr-only">(current)</span></a>
                  </li> -->
                </ul>
            </div>
        </nav>

        <div class="landing-page col-lg-12">
                <div class="lp-title-left col-lg-6 col-md-6 col-sm-6 col-8">
                    <a href="https://muslimapp.id/"><img src="assets/icons/muslimaoo.png" class="img-fluid logo-nav" alt="Responsive image"></a>
                    <h2 class="lp-title-left-h1"><?php echo $lptitlelefth1; ?></h2>
                    <h5 class="lp-title-left-h2"><?php echo $lptitlelefth2; ?></h5>
                    <table class="table table-stripped">
                        <tr>
                            <td class="icons-margin" style="border-top:none;padding:12.1px">
                                <div class="lp-icons col-lg-2 col-md-2">
                                    <a href="https://qurban.muslimapp.id" id="v-content-qurban">
                                        <img src="assets/icons/qurban@2x.png" class="lp-icon">
                                    </a>
                                    <h5><?php echo $lpicons2; ?></h5>
                                </div>
                            </td>
                            <td class="icons-margin" style="border-top:none;padding:12.1px">
                                <div class="lp-icons col-lg-2 col-md-2">
                                    <a href="https://donasi.muslimapp.id" id="v-content-donasi">
                                        <img src="assets/icons/donasi_icon.png" class="lp-icon">
                                    </a>
                                    <h5><?php echo $nav_donasi; ?></h5>
                                </div>
                            </td>
                            <td class="icons-margin" style="border-top:none;padding:12.1px">
                                <div class="lp-icons col-lg-2 col-md-2">
                                    <a href="https://booking.muslimapp.id" id="v-content-booking">
                                        <img src="https://cloud.muslimapp.id/public/image/bookingservice_ic.png" class="lp-icon" >
                                        <!-- style="margin-top:20px" -->
                                        
                                    </a>
                                    <h5><?php echo $nav_booking; ?></h5>
                                </div>
                            </td>
                            <td class="icons-margin" style="border-top:none;padding:12.1px">
                                <div class="lp-icons col-lg-2 col-md-2">
                                    <a href="https://store.muslimapp.id" id="v-content-store">
                                        <img src="assets/icons/islamic_product.png" class="lp-icon" >
                                        <!-- style="margin-top:20px" -->
                                    </a>
                                    <h5>Islamic Product</h5>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="icons-margin" style="border-top:none;padding:12.1px">
                                <div class="lp-icons col-lg-2 col-md-2" align="center">
                                    <a href="https://aqiqah.muslimapp.id" id="v-content-aqiqah">
                                    <img src="assets/icons/aqiqah@2x.png" class="lp-icon">
                                    </a>
                                    <h5><?php echo $lpicons; ?></h5>
                                </div>
                            </td>
                            <td class="icons-margin" style="border-top:none;padding:12.1px">
                                <div class="lp-icons col-lg-2 col-md-2">
                                    <a href="https://news.muslimapp.id" id="v-content-berita">
                                        <img src="assets/icons/News-Icon.png" class="lp-icon">
                                    </a>
                                    <h5><?php echo $nav_news; ?></h5>
                                </div>
                            </td>
                            <!-- <td class="icons-margin" style="border-top:none;padding:12.1px">
                                <div class="lp-icons col-lg-2 col-md-2">
                                    <a href="https://muslimapp.id/tarawih.php">
                                        <img src="assets/icons/calendar.png" class="lp-icon">
                                    </a>
                                    <h5>Tarawih</h5>
                                </div>
                            </td> -->
                            <td class="icons-margin" style="border-top:none;padding:12.1px">
                                <div class="lp-icons col-lg-2 col-md-2">
                                    <a href="https://muslimapp.id/imsak.php" id="v-content-imsak">
                                        <img src="assets/icons/qiblat@2x.png" class="lp-icon">
                                    </a>
                                    <h5>Imsak</h5>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="lp-btn col-lg-12 col-md-10 col-sm-4">
                        <div class="lp-btn-playstore col-lg-4 col-md-5">
                            <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp"><img src="assets/icons/playstore.png" class="img-fluid btn-playstore"></a>
                        </div>
                        <div class="lp-btn-appstore col-lg-4 col-md-5">
                            <a href="https://itunes.apple.com/us/app/muslimapp-id-qurban-aqiqah-etc/id1347060636?mt=8"><img src="assets/icons/appstore.png" class="img-fluid btn-appstore" alt="Responsive image"></a>
                        </div>
                    </div>
                </div>
        </div>

        <div class="lp-features col-lg-12">
            <div class="lp-features-title col-lg-5 col-md-5 col-sm-5 col-7 hideme">
                <h3 class="lp-features-h1"><?php echo $lpfeaturesh1; ?></h3>
                <h5 class="lp-features-h2"><?php echo $lpfeaturesh2; ?></h5>
            </div>
        </div>

        <div class="aqiqahtothenews col-lg-12">

            <div id="aqiqah" class="content-aqiqah col-lg-12">
                <div class="aqiqah-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="aqiqah-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='assets/icons/aqiqah@2x.png'>
                    </div>
                    <div class="aqiqah-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $Aqiqah; ?></h2>
                        <h5><?php echo $aqiqahh5; ?>
                        </h5>
                        <a href="https://aqiqah.muslimapp.id" class="btn-link"><?php echo $getnow; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 aqiqah-right hideme">
                    <img src='assets/images/aqiqah_illustration.png'>
                </div>
            </div>

            <div id="qurban" class="content-qurban col-lg-12 hideme">
                <div class="qurban-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="qurban-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='assets/icons/qurban@2x.png'>
                    </div>
                    <div class="qurban-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $Qurban; ?></h2>
                        <h5><?php echo $qurbanh5; ?>
                        </h5>
                        <a href="https://qurban.muslimapp.id" class="btn-link" style="width:fit-content;"><?php echo $qurbangn; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 qurban-right hideme">
                    <img src='assets/images/qurban_illustration.png'>
                </div>
            </div>

            <div id="islamic-product" class="content-aqiqah col-lg-12">
                <div class="aqiqah-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="aqiqah-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='assets/icons/islamic_product.png'>
                    </div>
                    <div class="aqiqah-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $islamic_product; ?></h2>
                        <h5><?php echo $islamic_product_desc; ?></h5>
                        <a href="https://store.muslimapp.id" class="btn-link"><?php echo $getnow_islamicproduct; ?></a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 aqiqah-right hideme">
                    <img src='assets/images/Shopping.png'>
                </div>
            </div>

            <div id="hajj-umroh" class="content-qurban col-lg-12 hideme">
                <div class="qurban-wrap col-lg-7 col-md-7 col-sm-6 col-11">
                    <div class="qurban-pic col-lg-4 col-sm-6 col-3 hideme">
                        <img src='assets/images/Artboard 1.png'>
                    </div>
                    <div class="qurban-title col-lg-7 col-md-8 col-sm-10 col-9 hideme">
                        <h2><?php echo $hajjumroh; ?></h2>
                        <h5><?php echo $hajjumrohh5; ?>
                        </h5>
                        <a href="https://booking.muslimapp.id" class="btn-link" style="width:fit-content;">
                            <?php echo $hajjbtn; ?>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-8 col-8 qurban-right hideme">
                    <img src='assets/images/Artboard 2.png'>
                </div>
            </div>

            <div class="our-features row">
                <div class="of-title hideme">
                    <h1><?php echo $feature; ?></h1>
                </div>
                <div class="of-content col-lg-11 col-md-11">
                    <div class="of-mockup col-lg-6 col-md-6 col-sm-5 col-8 hideme">
                        <img src='assets/images/mockup.png' class="of-mockup-img">
                    </div>
                    <div class="of-features col-lg-6 col-md-6 col-sm-8 col-11">
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/quran@2x.png" >
                            <p><?php echo $quran; ?></p>
                        </div>
                        <!-- <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/AsmaulHusna.png" >
                            <p><?php //echo $ah; ?></p>
                        </div> -->
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/aqiqah@2x.png">
                            <p><?php echo $aq; ?></p>
                        </div>
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/calendar.png">
                            <p><?php echo $kal; ?></p>
                        </div>
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/qurban@2x.png">
                            <p><?php echo $qur; ?></p>
                        </div>
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/adzan@2x.png">
                            <p><?php echo $js; ?></p>
                        </div>
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/qiblat@2x.png">
                            <p><?php echo $k; ?></p>
                        </div>
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/masjid@2x.png">
                            <p><?php echo $m; ?></p>
                        </div>
                        <!-- <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/AlMa'tsurat.png">
                            <p><?php //echo $as; ?></p>
                        </div> -->
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/images/god.png">
                            <p><?php echo $s; ?></p>
                        </div>
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/doa@2x.png">
                            <p><?php echo $d; ?></p>
                        </div>
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/tasbih.png">
                            <p><?php echo $t; ?></p>
                        </div>
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/alquran.png">
                            <p><?php echo $kh; ?></p>
                        </div>
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/live-mekah@2x.png">
                            <p><?php echo $lm; ?></p>
                        </div>
                        <!-- <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/voucher@2x.png">
                            <p><?php //echo $v; ?></p>
                        </div> -->
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/islamic_product.png">
                            <p><?php echo $ip; ?></p>
                        </div>
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/tracker.png">
                            <p class="gps-h-u"><?php echo $gps; ?></p>
                        </div>
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/icons/donasi_icon.png">
                            <p class="gps-h-u"><?php echo $donasi; ?></p>
                        </div>
                        <div class="feature col-lg-6 col-md-6 col-sm-6 col-6 hideme">
                            <img src="assets/images/Artboard 1.png">
                            <p class="gps-h-u"><?php echo $hajjumro2; ?></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container mt3 hideme">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <ul class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ul>

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <h3>Desti Tri Wulan</h3>
                        <p class="col-lg-6 col-md-8 col-sm-8 col-8">"<?php echo $tes1; ?>"</p>
                    </div>
                    <div class="carousel-item">
                        <h3>Yoyo Hudaya</h3>
                        <p class="col-lg-6 col-md-8 col-sm-8 col-8">"<?php echo $tes2; ?>"</p>
                    </div>
                    <div class="carousel-item">
                        <h3>Yandi Rama Krisna</h3>
                        <p class="col-lg-6 col-md-8 col-sm-8 col-8">"<?php echo $tes3; ?>"</p>
                    </div>
                </div>

                <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
                </div>
            </div>

            <div class="col-lg-12 insta-embeds" style="overflow:auto;">
                <div class="col-lg-4 insta-embed" style="float:left">
                    <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BrJ13EGgxSi/?utm_source=ig_embed&amp;utm_medium=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/BrJ13EGgxSi/?utm_source=ig_embed&amp;utm_medium=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div><div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div></a> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BrJ13EGgxSi/?utm_source=ig_embed&amp;utm_medium=loading" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">Sahabat Muslim, sekarang kamu bisa melakukan aqiqah dengan lebih mudah menggunakan MuslimApp. Layanan Aqiqah Online ini sudah tersedia di 15 kota, loh. Yuk, perkuat tali silaturahmi dan wujud syukur kepada Allah SWT. Dengan lebih mudah bersama MuslimApp. #GenggamKebaikan⁣ ⁣ ⁣ #muslimappid #muslimapp #muslim #apps #islam #moslem #sholat #shalat #solat #kiblat #makkah #kabah #aqiqah #like #love #follow #download #hajj #info #instagood #Indonesia #Islamic #dakwah #mohammad #hijab #pray #nature #travel #masjid⁣</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by <a href="https://www.instagram.com/muslimapp.id/?utm_source=ig_embed&amp;utm_medium=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px;" target="_blank"> Muslimapp.id</a> (@muslimapp.id) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2018-12-09T04:50:00+00:00">Dec 8, 2018 at 8:50pm PST</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>
                </div>
                <div class="col-lg-4 insta-embed" style="float:left">
                    <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BrH0fflgLjw/?utm_source=ig_embed&amp;utm_medium=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/BrH0fflgLjw/?utm_source=ig_embed&amp;utm_medium=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div><div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div></a> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BrH0fflgLjw/?utm_source=ig_embed&amp;utm_medium=loading" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">Sahabat Muslim, jangan lupa untuk sarapan sebelum melakukan aktivitas karena sarapan memiliki efek yang baik untuk tubuhmu. #GenggamKebaikan⁣ ⁣ ⁣ ⁣ #muslimappid #muslimapp #muslim #apps #islam #moslem #sholat #shalat #solat #kiblat #makkah #kabah #aqiqah #like #love #follow #download #hajj #info #instagood #Indonesia #Islamic #dakwah #mohammad #hijab #pray #info #tips #masjid</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by <a href="https://www.instagram.com/muslimapp.id/?utm_source=ig_embed&amp;utm_medium=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px;" target="_blank"> Muslimapp.id</a> (@muslimapp.id) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2018-12-08T09:59:34+00:00">Dec 8, 2018 at 1:59am PST</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>
                </div>
                <div class="col-lg-4 insta-embed" style="float:left">
                    <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BrFI-VXgDL1/?utm_source=ig_embed&amp;utm_medium=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/BrFI-VXgDL1/?utm_source=ig_embed&amp;utm_medium=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div><div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div></a> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BrFI-VXgDL1/?utm_source=ig_embed&amp;utm_medium=loading" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">Sahabat Muslim yang senang home décor bisa pakai kaligrafi buat mempercantik rumahmu, loh. Salah satu yang bisa dijadikan pilihan adalah kaligrafi yang diukir di kayu. #GenggamKebaikan . . . . . #muslimappid #muslimapp #muslim #apps #islam #moslem #sholat #shalat #solat #kiblat #makkah #kabah #aqiqah #like #love #follow #download #hajj #info #instagood #Indonesia #Islamic #dakwah #mohammad #hijab #pray</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by <a href="https://www.instagram.com/muslimapp.id/?utm_source=ig_embed&amp;utm_medium=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px;" target="_blank"> Muslimapp.id</a> (@muslimapp.id) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2018-12-07T09:00:49+00:00">Dec 7, 2018 at 1:00am PST</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>
                </div>
            </div>

            <div class="contact-us col-lg-12">
                <div class="cu-get-now col-lg-12 hideme">
                    <h3 class="col-lg-6"><?php echo $gt; ?></h3>
                    <div class="get-now col-lg-5 col-md-6 col-sm-8">
                        <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp" class="plays"><img src="assets/icons/playstore.png" class="col-lg-6 playstore"></a>
                        <a href="https://itunes.apple.com/us/app/muslimapp-id-qurban-aqiqah-etc/id1347060636?mt=8" class="apps"><img src="assets/icons/appstore.png" class="col-lg-6 appstore"></a>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="col-lg-3 footer-logo col-md-3 col-sm-5 col-6">
                <img src="assets/icons/muslimaoo.png" class="col-lg-9"><br/>
                <div id="idEA_trustmark" style="background-color:none">
                    <div> <!-- id="idEA_trustmark_wrapper" -->
                    <img id="idEA_trustmark_image" width="105px" />
                    </div>
                </div>
            </div>
            <div class="footer-contact col-lg-12 col-md-10">
                <h6 class="col-lg-12">info@muslimapp.id</h6>
                <div class="socialmedia col-lg-3">
                    <a href="https://www.facebook.com/muslimapp.id">
                        <img src="assets/icons/facebook.png">
                    </a>
                    <a href="https://www.twitter.com/muslimapp_id">
                        <img src="assets/icons/twitter.png">
                    </a>
                    <a href="https://www.instagram.com/muslimapp.id/">
                        <img src="assets/icons/ig.png">
                    </a>
                    <a href="https://www.youtube.com/channel/UCgrtp33wmhwS1Ci1UeJx8WA">
                        <img src="assets/icons/youtube.png">
                    </a>
                    <a href="https://www.linkedin.com/company/muslimapp/">
                        <img src="assets/icons/linkedin.png">
                    </a>
                </div>
                <div class="footer-menu col-lg-7">
                    <a href="syaratdanketentuan.php"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot1; ?></h6></a>
                    <a href="kebijakanprivasi.php"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot2; ?></h6></a>
                    <a href="https://api.whatsapp.com/send?phone=628112333724"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot3; ?></h6></a>
                    <a href="agreeaqiqah.php"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Aqiqah</h6></a>
                    <a href="agreequrban.php"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Qurban</h6></a>
                    <a href="agreeislamicproduct.php"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Islamic Product</h6></a>
                </div>
                <div class="col-lg-12 footer-copyright">
                    <p><?php echo $cp; ?></p>
                </div>
            </div>
        </footer>

        <div class="btn-drift col-lg-12">
            <div class="float-button-wrapper">
                <div class="float-button-page">
                    <a href="https://api.whatsapp.com/send?phone=628112333724">
                        <img src='assets/icons/whatsapp@2x.png'>
                        <h6><?php echo $con; ?></h6>
                    </a>
                </div>
            </div>
        </div>

    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Trustmark idEA Start -->
    <script type="text/javascript" src="https://www.idea.or.id/assets/js/trustmark_badge.js"></script>
    <link rel="stylesheet" href="https://www.idea.or.id/assets/css/trustmark_badge.css" />
    <input type="hidden" id="hid_trustmark_code" value="HCONLnpBGtqdSIVYse81aAWX"/>
    <input type="hidden" id="hid_base_url" value="https://www.idea.or.id/" /><input type="hidden" id="hid_alt" value="no"/>
    <div style="display:none">
    <div id="idEA_trustmark_verified">Verifikasi Hingga</div><div id="idEA_trustmark_until"></div>
    </div>
    <!-- Trustmark idEA End -->

    <script>
        $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            &&
            location.hostname == this.hostname
            ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                scrollTop: target.offset().top
                }, 1000, function() {
                // Callback after animation
                // Must change focus!
                var $target = $(target);
                $target.focus();
                if ($target.is(":focus")) { // Checking if the target was focused
                    return false;
                } else {
                    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                    $target.focus(); // Set focus again
                };
                });
            }
            }
        });

        $(document).ready(function() {

            /* Every time the window is scrolled ... */
            $(window).scroll( function(){

                /* Check the location of each desired element */
                $('.hideme').each( function(i){

                    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                    var bottom_of_window = $(window).scrollTop() + $(window).height();

                    /* If the object is completely visible in the window, fade it it */
                    if( bottom_of_window > bottom_of_object ){

                        $(this).animate({'opacity':'1'},500);

                    }

                });

            });

        });

        $(window).scroll(function() {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });

        $('#return-to-top').click(function() {      // When arrow is clicked
            $('body,html').animate({
                scrollTop : 0                       // Scroll to top of body
            }, 500);
        });

        document.getElementById('v-content-imsak').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_type: 'View Menu Imsak',
            });
        }, false);
        
        document.getElementById('v-content-berita').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'View Menu Berita',
            });
        }, false);

        document.getElementById('v-content-aqiqah').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'View Menu Aqiqah',
            });
        }, false);

        document.getElementById('v-content-qurban').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'View Menu Qurban',
            });
        }, false);

        document.getElementById('v-content-donasi').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'View Menu Donasi',
            });
        }, false);

        document.getElementById('v-content-booking').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'View Menu Booking Travel Hajj Umroh',
            });
        }, false);

        document.getElementById('v-content-store').addEventListener('click', function() {
            fbq('track', 'ViewContent', {
                content_ids: 'View Menu Store Muslimapp',
            });
        }, false);

    </script>

</body>
</html>

