<?php
if(isset($_GET["english"])){
	include "../assets/lang/english.php" ;
		}
else if(isset($_GET["indonesia"])){
	include "../assets/lang/indonesia.php" ;
	}
else{
	include "../assets/lang/indonesia.php";
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="shortcut icon" type="image/png" href="../assets/icons/favicon.png"/>
    <title>Qurban Aqiqah Muslimapp | All Muslim Needs In One App</title>
    <meta name="description" content="Aplikasi waktu sholat, Membaca Quran, Navigasi Arah Kiblat, Aqiqah Online dan Qurban ">
	<meta name="keywords" content="Aplikasi Muslim App, Waktu Sholat, Membaca Quran, Navigasi Arah Kiblat, Aqiqah Online, Qurban Online">
	<meta name="robots" content="index,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../assets/css/styless.css" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115158415-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115158415-1');
</script>
<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">window.dojoRequire(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us19.list-manage.com","uuid":"dd5b5b4fbe559b0d8576baa2b","lid":"a46458ad16","uniqueMethods":true}) })</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WL22FT6');</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '408538096676512');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=408538096676512&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Trustmark idEA Start -->
<!-- <script type="text/javascript" src="https://www.idea.or.id/../assets/js/trustmark_badge.js"></script><link rel="stylesheet" href="https://www.idea.or.id/../assets/css/trustmark_badge.css" /><input type="hidden" id="hid_trustmark_code" value="HCONLnpBGtqdSIVYse81aAWX"/><input type="hidden" id="hid_base_url" value="https://www.idea.or.id/" /><input type="hidden" id="hid_alt" value="no"/><div id="idEA_trustmark"><div id="idEA_trustmark_wrapper"><img id="idEA_trustmark_image" width="105px" /><div id="idEA_trustmark_verified">Verifikasi Hingga</div><div id="idEA_trustmark_until"></div></div></div> -->
<!-- Trustmark idEA End -->
</head>
<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WL22FT6"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a href="https://muslimapp.id" class="nav-link">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a href="https://muslimapp.id/aplikasi" class="nav-link">Aplikasi <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://qurban.muslimapp.id" class="nav-link">Qurban<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://aqiqah.muslimapp.id" class="nav-link">Aqiqah<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://muslimapp.id/Tracking/" class="nav-link"><?php echo $nav_order; ?><span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                      <a href="https://news.muslimapp.id/" class="nav-link"><?php echo $nav_news; ?><span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= $bahasa ?></a>
                    <div class="dropdown-menu backdropdown">
                        <a href="?indonesia" class="nav-link">ID<img src="../assets/icons/indo.png" class="icon-indo"><span class="sr-only">(current)</span></a>
                        <a href="?english" class="nav-link">EN<img src="../assets/icons/eng.png" class="icon-indo"><span class="sr-only">(current)</span></a>
                    </div>
                  </li>
                </ul>
            </div>
        </nav>

       

        <div class="lp-features col-lg-12">
            <div class="lp-features-title col-lg-5 col-md-5 col-sm-5 col-7" style="margin-top:30px">
                <h3 class="lp-features-h1" style="text-align:left">
                    <a href="https://muslimapp.id" style="color:#0b4624">Muslimapp.id</a>
                </h3>
                <h5 class="lp-features-h2" style="text-align:left">
                <?=$line1?>
                <br/><br/>
                <?=$line2?>
                <br/><br/>
                <?=$line3?>
<br/><br/>
<?=$line4?>
                </h5>
            </div>
        </div>

        <div class="aqiqahtothenews col-lg-12" style="margin-top:-200px">

        <div class="our-features row">
                <div class="of-title hideme">
                    <h1><?=$foot2?></h1>
                </div>
                <?php if(isset($_GET["english"])){ ?>
                    <div class="of-content col-lg-11 col-md-11">
                    <h6>
                   <b> 1.	Acquisition and Collection of User Data </b><br/>
                   Muslimapp.id mengumpulkan data pengguna dengan tujuan untuk memproses transaksi pengguna, mengelola dan memperlancar proses penggunaan aplikasi, serta tujuan-tujuan lainnya selama diizinkan oleh peraturan perundang-undangan yang berlaku. Adapun data pengguna yang dikumpulkan adalah sebagai berikut :<br/><br/>
                    <b>a)	Muslimapp.id collects user data with the aim to process user transactions, manage and expedite the process of using the application, as well as other purposes as long as permitted by applicable laws and regulations. <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The user data collected is as follows:<br/></b>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1)	Creating or updating a Muslimapp.id user id, including user name, e-mail address, telephone number, address, etc.<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2)	Creating or updating a Muslimapp.id user id, including user name, e-mail address, telephone number, address, etc.<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3)	Fill out the survey sent by Muslimapp.id or on behalf of Muslimapp.id;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4)	Interact with other users through messaging features, product discussions, reviews, and so on.<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5)	Using services in the application, including detailed transaction data, including type, amount and / or information of the product or service purchased, delivery address, method of payment used, transaction amount, date and time of <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;transaction, and other transaction details<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6)	Fill in payment data when the user performs payment transaction activities through the application, including but not limited to bank account data, credit cards, virtual accounts, instant payment, internet banking, <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;and / or<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7)	Uses features that require permission to access the user's device<br/><br/>

                    <b>b)	Data recorded when the user uses the application, including but not limited to:<br/></b>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1)	Real or approximate location data such as IP address, Wifi location, Geo Location, etc .;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2)	Data in the form of time for each user's activity, including registration, login, transaction, and so on<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3)	Data usage or user preferences, including user interaction in using applications, saved options, and selected settings.<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4)	The data is obtained using cookies, pixel tags, and similar technologies that create and maintain unique identifiers;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5)	Device data, including types of devices used to access applications, including hardware models, operating systems and versions, software, software, file names and versions, language options, unique device identifiers, serial numbers, <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;device movement information, and / or cellular network information;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6)	Data log (log), including records on the server that received data such as the device IP address, date and time of access, application features or pages viewed, application work processes and other system activities, and / or third party <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sites or services that users use before interacting with the application.
                    <br/><br/>

                    <b>c)	Data obtained from other sources, including :<br/></b>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1)	Muslimapp.id Business Partners who help Muslimapp.id in developing and presenting services in the application to users, including partners aqiqah service providers, qurban, sales of Hajj and Umrah pilgrimage equipment, and other<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;partners;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2)	Muslimapp.id Business Partners where users create or access Muslimapp.id accounts, such as social media services;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3)	Marketing service providers; <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4)	Publicly available sources.<br/>
                    <br/>
                    Muslimapp.id can combine data obtained from these sources with other data it has.
                    <br/><br/>

                    <b>2.	Data Usage</b><br/>
                    Muslimapp.id can use all or part of the data obtained and collected from users as mentioned in the previous section for the following matters:<br/><br/>
                    <b>a)	Process all forms of requests, activities and transactions carried out by users through the application, including for the purpose of sending products to users.<br/></b>
                    <b>b)	Provision of features to provide, realize, maintain and improve our products and services, including:<br/></b>
                    &nbsp;&nbsp;&nbsp;&nbsp;1)	Offering, obtaining, providing, or facilitating marketplace services and other products through applications;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;2)	Carry out internal activities needed to provide services to the Muslimapp.id application, such as solving software problems, operational problems, conducting data analysis, testing, and research, and to <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;monitor and analyze trends in usage and activities.<br/>
                    <b>c)	Help users when communicating with Muslimapp.id customer service, including to:<br/></b>
                    &nbsp;&nbsp;&nbsp;&nbsp;1)	Check and resolve user problems;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;2)	Direct user questions to the appropriate customer service officer to resolve the problem; and<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;3)	Oversee and improve the response of Muslimapp.id services.<br/>
                    <b>d)	Contact the user via email, letter, telephone, fax, etc., including but not limited to, to assist and / or complete the transaction process or the process of resolving the constraints.<br/></b>
                    <b>e)	Use information obtained from users for research, analysis, product development and testing purposes to improve the security and security of services on the application, as well as develop <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;new features and products.<br/></b>
                    <b>f)	Infsorming users about products, services, promotions, studies, surveys, news on the latest developments, events and others, both through the application and through other media, Muslimapp.id <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;can also use the information to promote relevant content about Muslimapp services .id and its business partners.<br/></b>
                    <b>g)	Carry out monitoring or investigation of suspicious transactions or transactions that indicate fraud or violation of the terms and conditions or applicable legal provisions, and take necessary <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;actions as a follow-up to the results of monitoring or investigating the transaction.<br/></b>
                    <b>h)	Under certain circumstances, Muslimapp.id may need to use or disclose user data for law enforcement purposes or to fulfill applicable legal and regulatory requirements, including in the event <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;of a dispute or legal process between the user and Muslimapp.id<br/></b>
                    <br/><br/>

                    <b>3.	Disclosure of User's Personal Data</b><br/>
                    Muslimapp.id guarantees that there will be no sale, transfer, distribution or loan of your personal data to other parties, without your permission, except in the following matters:<br/><br/>

                    a)	Required disclosure of user data to partners or other third parties who help Muslimapp.id in presenting services to the application and processing all forms of user activity in the application, <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;including processing transactions, verifying payments, shipping products, and others.<br/>
                    b)	Muslimapp.id can provide relevant information to business partners in accordance with the user's agreement to use business partner services, including applications that have integrated services,  <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;or business partners where Muslimapp.id has collaborated for specialized services.<br/>
                    c)	Communication between Muslimapp.id business partners and users is needed in terms of problem solving and other matters.<br/>
                    d)	Muslimapp.id can provide relevant information to vendors, consultants, marketing partners, firms, research, or similar service providers.<br/>
                    e)	Users contact Muslimapp.id through public media such as social media and certain features of the application, communication between users and Muslimapp.id may be seen in general.<br/>
                    f)	Muslimapp.id can share user information with affiliates to help provide services or perform data processing for and on behalf of Muslimapp.id.<br/>
                    g)	Muslimapp.id discloses user data in an effort to comply with legal obligations and / or legal requests from law enforcement officials.<br/>
                    <br/><br/>

                    <b>4.	Cookies </b><br/>
                    a)	Cookies are small files that will automatically take place in the user's device that performs the function of saving user preferences and configurations while logging in to the application.<br/>
                    b)	Cookies are not intended to be used when accessing other data that the user has on the user's computer, other than what the user has agreed to submit.<br/>
                    c)	Although the user's computer will automatically accept cookies, the user can choose to make modifications through the user's browser settings by choosing to reject cookies <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;(this option can limit the optimal service when accessing the application) <br/>
                    d)	Muslimapp.id uses Google Analytics Demographics and Interest features. The data we obtain from these features, such as age, gender, user interests, etc., will be used for the development of Muslimapp.id applications  <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;and content. if you don't want your data to be tracked by Google Analytics, users can use the Google Analytics Opt-Out Browser Add-On.<br/>
                    e)	Muslimapp.id can use features provided by third parties in order to improve the services and content of Muslimapp.id.<br/>
                    <br/>

                    <b>5. User Choice and Transparency</b><br/>
                    a)	Mobile devices in general (iOS, Android, etc.) have settings so that the Muslimapp.id application cannot access certain data without the consent of the user. The IOS device will give a notification to the user when the Muslimapp.id application <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;first requests access to the data, while the android device will give a notification to the user when the Muslimapp.id application is first loaded. By using the application and giving access to the application.<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp; The user is deemed to give his consent to the privacy policy.<br/>
                    b)	Users can access and change information in the form of email addresses, telephone numbers, address lists, payment methods and bank accounts through the settings feature on the application.<br/>
                    c)	Users can withdraw from information, notifications or private messages from administrators sent by Muslimapp.id. Muslimapp.id can still send messages or emails in the form of information <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;related to user account information and transactions.<br/>
                    d)	d)	To the extent permitted by applicable regulations, users can contact Muslimapp.id to withdraw approval of the acquisition, collection, storage, management and use of user data. If this happens, the user understands the consequences  <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;that the user cannot use Muslimapp.id application services or other services.<br/>
                    <br/>

                    <b>6.	Storage and Deletion of Information</b><br/>
                    Muslimapp.id will store information as long as the user account remains active and can carry out deletion in accordance with the provisions of applicable law<br/>
                    <br/>

                    <b>7.	Privacy Policy Updates</b> <br/>
                    Muslimapp.id can change or update this privacy policy at any time. Muslimapp.id recommends that users read carefully and check this privacy policy page from time to time to find out any changes. By continuing to access and use the Muslimapp.id application service and other services, users are considered to agree to changes in privacy policies.
                    <br/>
                    </h6>
                </div>
                <?php } else { ?>
                <div class="of-content col-lg-11 col-md-11">
                    <h6>
                   <b> 1.	Perolehan dan Pengumpulan Data Pengguna </b><br/>
                   Muslimapp.id mengumpulkan data pengguna dengan tujuan untuk memproses transaksi pengguna, mengelola dan memperlancar proses penggunaan aplikasi, serta tujuan-tujuan lainnya selama diizinkan oleh peraturan perundang-undangan yang berlaku. Adapun data pengguna yang dikumpulkan adalah sebagai berikut :<br/><br/>
                    <b>a)	Data yang diserahkan secara mandiri oleh pengguna, termasuk namun tidak terbatas pada data yang diserahkan pada saat pengguna :<br/></b>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1)	Membuat atau memperbarui user id Muslimapp.id, termasuk diantaranya nama pengguna, alamat email, nomor telepon, alamat, dan lain-lain;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2)	Menghubungi Muslimapp.id, termasuk melalui layanan konsumen;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3)	Mengisi survey yang dikirimkan oleh Muslimapp.id atau atas nama Muslimapp.id;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4)	Melakukan interaksi dengan pengguna lainnya melalui fitur pesan, diskusi produk, ulasan, dan sebagainya.<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5)	Mempergunakan layanan-layanan pada aplikasi, termasuk data transaksi yang detail, diantaranya jenis, jumlah dan/atau keterangan dari produk atau layanan yang dibeli, alamat pengiriman, cara pembayaran yang digunakan, jumlah 
                    <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;transaksi, tanggal dan waktu transaksi, serta detail transaksi lainnya;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6)	Mengisi data-data pembayaran pada saat pengguna melakukan aktivitas transaksi pembayaran melalui aplikasi, termasuk namun tidak terbatas pada data rekening bank, kartu kredit, virtual account, instant payment, internet banking, <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dan/atau <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7)	Menggunakan fitur yang membutuhkan izin akses terhadap perangkat pengguna<br/><br/>

                    <b>b)	Data yang terekam pada saat pengguna mempergunakan aplikasi, termasuk namun tidak terbatas pada :<br/></b>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1)	Data lokasi riil atau perkiraannya seperti alamat IP, lokasi Wifi, Geo Location, dan sebagainya;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2)	Data berupa waktu dari setiap aktivitas pengguna, termasuk aktivitas pendaftaran, login, transaksi, dan lain sebagainya;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3)	Data penggunaan atau preferensi pengguna, diantaranya interaksi pengguna dalam menggunakan aplikasi, pilihan yang disimpan, serta pengaturan yang dipilih. Data tersebut diperoleh menggunakan cookies, pixel tags, dan teknologi <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;serupa yang menciptakan dan mempertahankan pengenal unik;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4)	Data perangkat, diantaranya jenis perangkat yang digunakan untuk mengakses aplikasi, termasuk model hardware, system operasi dan versinya, perangkat lunak, software, nama file dan versinya, pilihan Bahasa, pengenal perangkat <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;unik, nomor seri, informasi gerakan perangkat, dan/atau informasi jaringan selular;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5)	Data catatan (log), diantaranya catatan pada server yang menerima data seperti alamat IP perangkat, tanggal dan waktu akses, fitur aplikasi atau laman yang dilihat, proses kerja aplikasi dan aktivitas system lainnya, dan/atau situs atau <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;layanan pihak ketiga yang pengguna gunakan sebelum berinteraksi dengan aplikasi.<br/><br/>

                    <b>c)	Data yang diperoleh dari sumber lain, termasuk :<br/></b>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1)	Mitra Usaha Muslimapp.id yang turut membantu Muslimapp.id dalam mengembangkan dan menyajikan layanan-layanan dalam aplikasi kepada pengguna, antara lain mitra penyedia layanan aqiqah, qurban, penjualan perlengkapan <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ibadah haji dan umroh, dan mitra-mitra lainnya;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2)	Mitra Usaha Muslimapp.id tempat pengguna membuat atau mengakses akun Muslimapp.id, seperti layanan media social;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3)	Penyedia layanan pemasaran;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4)	Sumber yang tersedia secara umum.<br/>
                    <br/>
                    Muslimapp.id dapat menggabungkan data yang diperoleh dari sumber tersebut dengan data lain yang dimilikinya.
                    <br/><br/>

                    <b>2.	Penggunaan data</b><br/>
                    Muslimapp.id dapat menggunakan keseluruhan atau sebagian data yang diperoleh dan dikumpulkan dari pengguna sebagaimana disebutkan dalam bagian sebelumnya untuk hal-hal sebagai berikut :<br/><br/>
                    <b>a)	Memproses segala bentuk permintaan, aktivitas maupun transaksi yang dilakukan oleh pengguna melalui aplikasi, termasuk untuk keperluan pengiriman produk kepada pengguna.<br/></b>
                    <b>b)	Penyediaan fitur-fitur untuk memberikan, mewujudkan, memelihara dan memperbaiki produk dan layanan kami, termasuk :<br/></b>
                    &nbsp;&nbsp;&nbsp;&nbsp;1)	Menawarkan, memperoleh, menyediakan, atau memfasilitasi layanan marketplace maupun produk-produk lainnya melalui aplikasi;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;2)	Melakukan kegiatan internal yang diperlukan untuk menyediakan layanan pada aplikasi Muslimapp.id, seperti pemecahan masalah software, permasalahan operasional, melakukan analisis data, pengujian, dan penelitian, dan untuk <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;memantau dan menganalisis kecenderungan penggunaan dan aktivitas.<br/>
                    <b>c)	Membantu pengguna pada saat berkomunikasi dengan layanan pelanggan Muslimapp.id, diantaranya untuk :<br/></b>
                    &nbsp;&nbsp;&nbsp;&nbsp;1)	Memeriksa dan mengatasi permasalahan pengguna;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;2)	Mengarahkan pertanyaan pengguna kepada petugas layanan pelanggan yang tepat untuk mengatasi permaslaahan; dan<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;3)	Mengawasi dan memperbaiki tanggapan layanan Muslimapp.id.<br/>
                    <b>d)	Menghubungi pengguna melalui email, surat, telepon, fax, dan lain-lain, termasuk namun tidak terbatas, untuk membantu dan/atau menyelesaikan proses transaksi maupun proses penyelesaian kendala.<br/></b>
                    <b>e)	Menggunakan informasi yang diperoleh dari pengguna untuk tujuan penelitian, analisis, pengembangan dan pengujian produk guna meningkatkan keamanan dan keamanan layanan-layanan pada aplikasi, serta mengembangkan <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fitur dan produk baru.<br/></b>
                    <b>f)	Menginformasikan kepada pengguna terkait produk, layanan, promosi, studi, survey, berita perkembangan terbaru, acara dan lain-lain, baik melalui aplikasi maupun melalui media lainnya, Muslimapp.id juga dapat menggunakan <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;informasi tersebut untuk mempromosikan konten yang relevan tentang layanan Muslimapp.id dan mitra usahanya.<br/></b>
                    <b>g)	Melakukan monitoring ataupun investigasi terhadap transaksi-transaksi mencurigakan atau transaksi yang terindikasi mengandung unsur kecurangan atau pelanggaran terhadap syarat dan ketentuan atau ketentuan hukum yang <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;berlaku, serta melakukan tindakan-tindakan yang diperlukan sebagai tindak lanjut dari hasil monitoring atau investigasi transaksi tersebut.<br/></b>
                    <b>h)	Dalam keadaan tertentu, Muslimapp.id mungkin perlu untuk menggunakan ataupun mengungkapkan data pengguna untuk tujuan penegakkan hukum atau untuk pemenuhan persyaratan hukum dan peraturan yang berlaku, <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;termasuk dalam hal terjadinya sengketa atau proses hukum antara pengguna dan Muslimapp.id.<br/></b>
                    <br/><br/>

                    <b>3.	Pengungkapan Data Pribadi Pengguna</b><br/>
                    Muslimapp.id menjamin tidak ada penjualan, pengalihan, distribusi atau meminjamkan data pribadi anda kepada pihak lain, tanpa seizin dari anda, kecuali dalam hal-hal sebagai berikut :<br/><br/>

                    a)	Dibutuhkan adanya pengungkapan data pengguna kepada mitra atau pihak ketiga lain yang membantu Muslimapp.id dalam menyajikan layanan pada aplikasi dan memproses segala bentuk aktivitas pengguna dalam aplikasi, termasuk <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;memproses transaksi, verifikasi pembayaran, pengiriman produk, dan lain-lain.<br/>
                    b)	Muslimapp.id dapat menyediakan informasi yang relevan kepada mitra usaha sesuai dengan persetujuan pengguna untuk menggunakan layanan mitra usaha, termasuk diantaranya aplikasi yang telah saling mengintegrasikan layanannya, <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;atau mitra usaha yang mana Muslimapp.id telah bekerjasama untuk layanan yang dikhususkan.<br/>
                    c)	Dibutuhkan adanya komunikasi antara mitra usaha Muslimapp.id dengan pengguna dalam hal penyelesaian kendala maupun hal-hal lainnya.<br/>
                    d)	Muslimapp.id dapat menyediakan informasi yang relevan kepada vendor, konsultan, mitra pemasaran, firma, riset, atau penyedia layanan sejenis.<br/>
                    e)	Pengguna menghubungi Muslimapp.id melalui media public seperti, media social dan fitur tertentu pada aplikasi, komunikasi antar pengguna dan Muslimapp.id mungkin dapat dilihat secara umum.<br/>
                    f)	Muslimapp.id dapat membagikan informasi pengguna kepada afiliasinya untuk membantu memberikan layanan atau melakukan pengolahan data untuk dan atas nama Muslimapp.id.<br/>
                    g)	Muslimapp.id mengungkapkan data pengguna dalam upaya mematuhi kewajiban hukum dan/atau adanya permintaan yang sah dari apparat penegak hukum.<br/>
                    <br/><br/>

                    <b>4.	Cookies </b><br/>
                    a)	Cookies adalah file kecil yang secara otomatis akan mengambil tempat di dalam perangkat pengguna yang menjalankan fungsi dalam menyimpan preferensi maupun konfigurasi pengguna selama log in ke aplikasi.<br/>
                    b)	Cookies tersebut tidak diperuntukkan untuk digunakan pada saat melakukan akses data lain yang pengguna miliki di perangkat computer pengguna, selain dari yang telah pengguna setujui untuk disampaikan<br/>
                    c)	Walaupun secara otomatis perangkat computer pengguna akan menerima cookies, pengguna dapat menentukan pilihan untuk melakukan modifikasi melalui pengaturan browser pengguna yaitu dengan memilih untuk menolak cookies <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;(pilihan ini dapat membatasi layanan optimal pada saat melakukan akses ke aplikasi)<br/>
                    d)	Muslimapp.id menggunakan fitur Google Analytics Demographics and Interest. Data yang kami peroleh dari fitur tersebut, seperti umur, jenis kelamin, minat pengguna dan lain-lain, akan kami gunakan untuk pengembangan aplikasi dan <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;konten Muslimapp.id. jika tidak ingin data anda terlacak oleh Google Analytics, pengguna dapat menggunakan Add-On Google Analytics Opt-Out Browser.<br/>
                    e)	Muslimapp.id dapat menggunakan fitur-fitur yang disediakan oleh pihak ketiga dalam rangka meningkatkan layanan dan konten Muslimapp.id.<br/>
                    <br/>

                    <b>5.	Pilihan Pengguna dan Transparansi</b><br/>
                    a)	Perangkat seluler pada umumnya (ios, Android, dan sebagainya) memiliki pengaturan sehingga aplikasi Muslimapp.id tidak dapat mengakses data tertentu tanpa persetujuan dari pengguna. Perangkat IOS akan memberikan <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;pemberitahuan kepada pengguna saat aplikasi Muslimapp.id pertama kali meminta akses terhadap data tersebut, sedangkat perangkat android akan memberikan pemberitahuan kepada pengguna saat aplikasi Muslimapp.id pertama kali <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;dimuat. Dengan menggunakan aplikasi dan memberikan akses terhadap aplikasi. Pengguna dianggap memberikan persetujuannya terhadap kebijakan privasi.<br/>
                    b)	Pengguna dapat mengakses  dan mengubah informasi berupa alamat email, nomor telephone, daftar alamat, metode pembayaran dan rekening bank melalui fitur pengaturan pada aplikasi.<br/>
                    c)	Pengguna dapat menarik diri dari informasi, notifikasi atau pesan pribadi dari administrator yang dikirimkan oleh Muslimapp.id. Muslimapp.id tetap dapat mengirimkan pesan atau email berupa keterangan terkait informasi akun <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;pengguna dan transaksi.<br/>
                    d)	Sejauh diizinkan oleh ketentuan yang berlaku, pengguna dapat menghubungi Muslimapp.id untuk melakukan penarikan persetujuan terhadap perolehan, pengumpulan, penyimpanan, pengelolaan dan penggunaan data pengguna. <br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;Apabila terjadi demikian maka pengguna memahami konsekuensi bahwa pengguna tidak dapat menggunakan layanan aplikasi Muslimapp.id maupun layanan lainnya. <br/>
                    <br/>

                    <b>6.	Penyimpanan dan Penghapusan Informasi</b><br/>
                    Muslimapp.id akan menyimpan informasi selama akun pengguna tetap aktif dan dapat melakukan penghapusan sesuai dengan ketentuan peraturan hukum yang berlaku<br/>
                    <br/>

                    <b>7.	Pembaruan Kebijakan Privasi</b> <br/>
                    Muslimapp.id dapat sewaktu-waktu melakukan perubahan atau pembaruan terhadap kebijakan privasi ini. Muslimapp.id menyarankan agar pengguna membaca secara seksama dan memeriksa halaman kebijakan privasi ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan aplikasi Muslimapp.id maupun layanan lainnya, maka pengguna dianggap menyetujui perubahan-perubahan dalam kebijakan privasi.
                    <br/>
                    </h6>
                </div>
                <?php } ?>

            </div>


            <div class="contact-us col-lg-12">
                <div class="cu-get-now col-lg-12 hideme">
                    <h3 class="col-lg-6"><?php echo $gt; ?></h3>
                    <div class="get-now col-lg-5 col-md-6 col-sm-8">
                        <a href="https://play.google.com/store/apps/details?id=com.dmg.muslimapp" class="plays"><img src="../assets/icons/playstore.png" class="col-lg-6 playstore"></a>
                        <a href="https://itunes.apple.com/us/app/muslimapp-id-qurban-aqiqah-etc/id1347060636?mt=8" class="apps"><img src="../assets/icons/appstore.png" class="col-lg-6 appstore"></a>
                    </div>
                </div>
            </div>

        </div>

        <footer>
            <div class="col-lg-3 footer-logo col-md-3 col-sm-5 col-6">
                <img src="../assets/icons/muslimaoo.png" class="col-lg-9"><br/>
                <div id="idEA_trustmark" style="background-color:none">
                    <div> <!-- id="idEA_trustmark_wrapper" -->
                    <img id="idEA_trustmark_image" width="105px" />
                    </div>
                </div>
            </div>
            <div class="footer-contact col-lg-12 col-md-10">
                <h6 class="col-lg-12">info@muslimapp.id</h6>
                <div class="socialmedia col-lg-3">
                    <a href="https://www.facebook.com/muslimapp.id">
                        <img src="../assets/icons/facebook.png">
                    </a>
                    <a href="https://www.twitter.com/muslimapp_id">
                        <img src="../assets/icons/twitter.png">
                    </a>
                    <a href="https://www.instagram.com/muslimapp.id/">
                        <img src="../assets/icons/ig.png">
                    </a>
                    <a href="https://www.youtube.com/channel/UCgrtp33wmhwS1Ci1UeJx8WA">
                        <img src="../assets/icons/youtube.png">
                    </a>
                    <a href="https://www.linkedin.com/company/muslimapp/">
                        <img src="../assets/icons/linkedin.png">
                    </a>
                </div>
                <div class="footer-menu col-lg-7">
                    <a href="https://muslimapp.id/syaratdanketentuan"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot1; ?></h6></a>
                    <a href="https://muslimapp.id/kebijakanprivasi"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot2; ?></h6></a>
                    <a href="https://api.whatsapp.com/send?phone=628112333724"><h6 class="col-lg-4 col-md-4 col-sm-4"><?php echo $foot3; ?></h6></a>
                    <a href="https://muslimapp.id/agreeaqiqah"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Aqiqah</h6></a>
                    <a href="https://muslimapp.id/agreequrban"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Qurban</h6></a>
                    <a href="https://muslimapp.id/agreeislamicproduct"><h6 class="col-lg-4 col-md-4 col-sm-4">User Agreement Islamic Product</h6></a>
                </div>
                <div class="col-lg-12 footer-copyright">
                    <p><?php echo $cp; ?></p>
                </div>
            </div>
        </footer>

        <div class="btn-drift col-lg-12">
            <div class="float-button-wrapper">
                <div class="float-button-page">
                    <a href="https://api.whatsapp.com/send?phone=628112333724">
                        <img src='../assets/icons/whatsapp@2x.png'>
                        <h6><?php echo $con; ?></h6>
                    </a>
                </div>
            </div>
        </div>

    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Trustmark idEA Start -->
    <script type="text/javascript" src="https://www.idea.or.id/../assets/js/trustmark_badge.js"></script>
    <link rel="stylesheet" href="https://www.idea.or.id/../assets/css/trustmark_badge.css" />
    <input type="hidden" id="hid_trustmark_code" value="HCONLnpBGtqdSIVYse81aAWX"/>
    <input type="hidden" id="hid_base_url" value="https://www.idea.or.id/" /><input type="hidden" id="hid_alt" value="no"/>
    <div style="display:none">
    <div id="idEA_trustmark_verified">Verifikasi Hingga</div><div id="idEA_trustmark_until"></div>
    </div>
    <!-- Trustmark idEA End -->

    <script>
        $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            &&
            location.hostname == this.hostname
            ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                scrollTop: target.offset().top
                }, 1000, function() {
                // Callback after animation
                // Must change focus!
                var $target = $(target);
                $target.focus();
                if ($target.is(":focus")) { // Checking if the target was focused
                    return false;
                } else {
                    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                    $target.focus(); // Set focus again
                };
                });
            }
            }
        });

        $(document).ready(function() {

            /* Every time the window is scrolled ... */
            $(window).scroll( function(){

                /* Check the location of each desired element */
                $('.hideme').each( function(i){

                    var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                    var bottom_of_window = $(window).scrollTop() + $(window).height();

                    /* If the object is completely visible in the window, fade it it */
                    if( bottom_of_window > bottom_of_object ){

                        $(this).animate({'opacity':'1'},500);

                    }

                });

            });

        });

        $(window).scroll(function() {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });

        $('#return-to-top').click(function() {      // When arrow is clicked
            $('body,html').animate({
                scrollTop : 0                       // Scroll to top of body
            }, 500);
        });
    </script>

</body>
</html>
