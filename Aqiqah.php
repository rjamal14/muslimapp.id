<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aqiqah extends GMN_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->module('Models/Model');
		$this->load->library('session');
		 $this->load->library('form_validation');
	}

	public function index(){
		$this->session->sess_destroy();
		$current['current']=$this->uri->segment(2);
		$current['tgl']=date("Y-m-d h:i:s");
		$current['title']='Order Aqiqah | Muslimapp.id';
		$this->load->view('Models/Admin/header',$current);
		$this->load->view('Models/Admin/menu');
		$this->load->view('Aqiqah/Aqiqah');
		$this->load->view('Models/Admin/footer');
		$this->load->view('Aqiqah/Aqiqah-js');
	}

	public function select_kota(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$term = trim(strip_tags($_GET['q']));
		if(empty($_GET['q'])){
			$result = $this->model_app->Select_tb("regencies");
		}else{
			$result = $this->model_app->Select_like_or("regencies", array("id"=>$term), array("name"=>$term), array("id !"=>0));
		}
		$json=array();
		foreach($result as $row){
			$json[] = ['id'=>$row['id'], 'text'=>$row['id'] ." - ". $row['name']];
		}
		echo json_encode($json);
	}

	public function list_paket(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$post=$this->input->post(NULL, TRUE);
		$this->db->where(array('kota'=>$post['kota'], 'status_delete'=>(0 or NULL)));
		$data_res = $this->db->order_by("harga_jual", "asc")->get('paket_aqiqah')->result();
		echo json_encode($data_res);
	}

	public function save_session(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		  $config = array(
			array('field' => 'nama_pemesan','rules' => 'required'),
			array('field' => 'nama_ayah','rules' => 'required'),
			array('field' => 'nama_ibu','rules' => 'required'),
			array('field' => 'nama_anak','rules' => 'required'),
			array('field' => 'no_hp','rules' => 'required'),
			array('field' => 'email','rules' => 'required'),
			array('field' => 'tgl_kirim','rules' => 'required'),
			array('field' => 'alamat','rules' => 'required'),
		  );

		 $this->form_validation->set_rules($config);
		  if ($this->form_validation->run() === TRUE) {
			$post=$this->input->post(NULL, TRUE);
			//echo $post['nama_pemesan'];
			$datas = json_decode($post['id_paket'], true);
			//print_r($datas);

			$dataOrder = array(
							"nama_pemesan" => $post['nama_pemesan'],
							"nama_ayah" => $post['nama_ayah'],
							"nama_ibu" => $post['nama_ibu'],
							"nama_anak" => $post['nama_anak'],
							"no_hp" => $post['no_hp'],
							"email" => $post['email'],
							"tgl_kirim" => $post['tgl_kirim'] . " 10:00",
							"id_kota" => $datas[0][4],
							"alamat" => $post['alamat'],
							"note" => $post['catatan'],
							"diskon_kode" => $post['kode_promo'],
							"bank_tujuan" => $post['bank'],
							"device_id" => "",
							"id_user" => "",
							"lat" => "",
							"long" => "",
							"referral_code" => ""
			);
			$dataOrder['paket']= array();
			foreach($datas as $row){
				$dataOrder['paket'][] = array(
								"id_paket" => $row[0],
								"qty" => $row[3]
				);
			}

			//echo json_encode($dataOrder);
			$data_string = json_encode($dataOrder);

			$ch = curl_init('https://pro.muslimapp.id/restapi/Aqiqah/Order_aqiqah/');
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Authorization: Basic d2ViMTp3ZWIx'
				));
				$result = curl_exec($ch);
				$response = json_decode($result, true);
				//print_r($result);
				if($response['status'] == 'success'){
					echo json_encode(
									array(
										"message"=>"Pesanan berhasil dikirim. Silahkan cek sms atau email untuk konfirmasi pembayaran.",
										"class"=>"success",
										"status"=>"success",
									)
							);
				}else {
					echo json_encode(
									array(
										"message"=> "Terjadi kesalahan. Silahkan diulangi lagi.",
										"class"=>"error",
										"status"=>"error",
									)
							);
				}

		  }else {
			echo json_encode(
								array(
									"message"=> "Form wajib diisi",
									"class"=>"error",
									"status"=>"error",
								)
			);
		  }

	}

	public function save_session2(){

			if (!$this->input->is_ajax_request()) {
			   exit('No direct script access allowed');
			}

		  $config = array(
			array('field' => 'nama_pemesan','rules' => 'required'),
			array('field' => 'nama_ayah','rules' => 'required'),
			array('field' => 'nama_ibu','rules' => 'required'),
			array('field' => 'nama_anak','rules' => 'required'),
			array('field' => 'no_hp','rules' => 'required'),
			array('field' => 'email','rules' => 'required'),
			array('field' => 'tgl_kirim','rules' => 'required'),
			array('field' => 'alamat','rules' => 'required'),
		  );

			$this->form_validation->set_rules($config);
		if ($this->form_validation->run() === TRUE) {

			$post=$this->input->post(NULL, TRUE);
			// print_r(json_encode($post['postData']));

			// $datas = json_encode($post['postData']);

			// print_r($datas);

// ($post['postData']['pemesan'][9])?$post['postData']['pemesan'][9]:
			$dataOrder = array(
							"nama_pemesan" => $post['postData']['pemesan'][0],
							"nama_ayah" => $post['postData']['pemesan'][1],
							"nama_ibu" => $post['postData']['pemesan'][2],
							"nama_anak" => $post['postData']['pemesan'][3],
							"no_hp" => $post['postData']['pemesan'][4],
							"email" => $post['postData']['pemesan'][5],
							"tgl_kirim" => $post['postData']['pemesan'][6] . " 10:00",
							"id_kota" => $post['postData']['id_paket'][0][4],
							"alamat" => $post['postData']['alamat'],
							"note" => $post['postData']['pemesan'][7],
							"diskon_kode" => $post['postData']['diskon_kode'],
							"bank_tujuan" => $post['postData']['pemesan'][8],
							"device_id" => "",
							"id_user" => "",
							"lat" => "",
							"long" => "",
							"referral_code" => ""
			);
			$dataOrder['paket']= array();
			foreach($post['postData']['id_paket'] as $row){
				$dataOrder['paket'][] = array(
								"id_paket" => $row[0],
								"qty" => $row[3]
				);
			}
			// print_r(json_encode($dataOrder));

			//echo json_encode($dataOrder);
			$data_string = json_encode($dataOrder);
			print_r($data_string);

			$ch = curl_init('https://pro.muslimapp.id/restapi/Aqiqah/Order_aqiqah/');
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Authorization: Basic d2ViMTp3ZWIx'
				));
				$result = curl_exec($ch);
				$response = json_decode($result, true);
				//print_r($result);
				if($response['status'] == 'success'){
					echo json_encode(
									array(
										"message"=>"Pesanan berhasil dikirim. Silahkan cek sms atau email untuk konfirmasi pembayaran.",
										"class"=>"success",
										"status"=>"success",
									)
							);
				}else {
					echo json_encode(
									array(
										"message"=> "Terjadi kesalahan. Silahkan diulangi lagi.",
										"class"=>"error",
										"status"=>"error",
									)
							);
				}

		}else {
		echo json_encode(
							array(
								"message"=> "Form wajib diisi",
								"class"=>"error",
								"status"=>"error",
							)
		);
		}
	}

	public function set_alamat(){
		$post=$this->input->post(NULL, TRUE);
		$this->session->set_userdata("alamat", $post['alamat']);
		$this->session->set_userdata("lat", $post['lat']);
		$this->session->set_userdata("long", $post['long']);
		$data=$this->session->userdata("alamat");
		echo json_encode(array("status"=>"success", "data"=>$data));
	}

	function getBank(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$uri = 'http://159.65.14.198/restapi/Bank/List_bank';
		$ch = curl_init($uri);
		curl_setopt_array($ch, array(
			CURLOPT_HTTPHEADER	=> array('Authorization: Basic YWRtaW46MzEwYjU2NzhlZWNlNjMzMzZlNDY5OGQyNzIyYWFkOTE='),
			CURLOPT_RETURNTRANSFER	=>true,
			CURLOPT_VERBOSE		=> 1
		));
		$out = curl_exec($ch);
		curl_close($ch);

		$dataBank = json_decode($out, true);
		foreach($dataBank["data"] as $singleData){
			$json[] = ['ID'=>$singleData['id'],
			'Logo'=> $singleData['logo'],
			'Atas_nama'=> $singleData['atas_nama'],
			'Name'=> $singleData['nama_bank'],
			'Norek' => $singleData['no_rek_bank']
		];
		}
		echo json_encode($json);
	}

	public function checkPromo2() {

		$post=$this->input->post(NULL, TRUE);
		$json_data = json_decode($post['paket'], true);
		//echo $json_data[0][1];
		foreach($json_data as $row){
			$data['data'][]=array(
					"paket"=>$json_data[0][0],
					"code"=>$json_data[0][1]
			);
		}
		echo json_encode($data);

	}

	public function checkPromo(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$post=$this->input->post(NULL, TRUE);
		$json_data = json_decode($post['paket'], true);

		$counter = 0;
		$total_diskon = 0;
		$qty = array();

		$i=0;
		foreach($json_data as $row){
			$qty[] = $row[2];
			$data['data'][]=array(
					"paket"=>$json_data[0][0],
					"code"=>$json_data[0][1]
			);

			$data_string = json_encode($data);

			$ch = curl_init('https://pro.muslimapp.id/restapi/Aqiqah/QP_code/');
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Authorization: Basic d2ViMTp3ZWIx'
			));
			$result = curl_exec($ch);
			$data_promo = json_decode($result, true);
			if($data_promo['data'][0]['status'] == 'Not-valid'){
				$counter = $counter + 0;
				$total_diskon = $total_diskon + 0;
				//echo json_encode(array("status"=>"error", "class"=>"error", "message"=> "Tidak ditemukan"));
			}else {
				$counter = $counter + 1;
					$total_diskon = $total_diskon + ($data_promo['data'][0]['diskon']*$qty[$i]);
				//echo json_encode(array("status"=>"success", "class"=>"success", "message"=> $data_promo['data'][0]['diskon']));
			}
			$i++;
		}
		//echo $counter;
		//echo $total_diskon;

		if($counter == 0) {
			echo json_encode(array("status"=>"error", "class"=>"error", "message"=> "Tidak ditemukan"));
		} else {
			echo json_encode(array("status"=>"success", "class"=>"success", "message"=> $total_diskon));
		}

		// $data_string = json_encode($data);

		// $ch = curl_init('https://dev.muslimapp.id/restapi/Aqiqah/QP_code/');
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			// 'Content-Type: application/json',
			// 'Authorization: Basic d2ViMTp3ZWIx'
		// ));
		// $result = curl_exec($ch);
		// $data_promo = json_decode($result, true);
		//print_r($data_promo);
		// if($data_promo['data'][0]['status'] == 'Not-valid'){
			// echo json_encode(array("status"=>"error", "class"=>"error", "message"=> "Tidak ditemukan"));
		// }else {
			// echo json_encode(array("status"=>"success", "class"=>"success", "message"=> $data_promo['data'][0]['diskon']));
		// }
		//$data["myArray_kota"] = $myArray_kota;
	}

	public function list_paket2(){
		$post=$this->input->post(NULL, TRUE);
		$this->db->where(array('kota'=>$post['kota'], 'status_delete'=>(0 or NULL)));
		$data_res = $this->db->order_by("harga_jual", "asc")->get('paket_aqiqah')->result();
		echo json_encode($data_res);
	}


}
