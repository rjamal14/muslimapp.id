<?php
$nav_mitra = "Mitra";
$nav_order = "Cek Order";
$nav_news = "Berita";
$nav_donasi = "Donasi";
$nav_booking = "Haji Umroh";
$bahasa = 'Bahasa';

$lptitlelefth1 = "Aplikasi Ibadah muslimapp.id - Gratis dan Tanpa Iklan";
$lptitlelefth2 = "Muslimapp merupakan aplikasi yang membantu umat muslim untuk melakukan ibadah hanya dalam satu genggaman saja.";

$lpicons = "Aqiqah";
$lpicons2 = "Qurban";
$lpicons3 = "Wedding";
$lpicons4 = "Islamic Products";
$detail = "Selengkapnya";

$lpfeaturesh1 = "Fitur Aplikasi Muslimapp.id";
$lpfeaturesh2 = "Muslimapp.id adalah aplikasi adzan otomatis, jadwal sholat, hingga aqiqah-qurban online. Aplikasi yang membantu ibadah menjadi lebih mudah. Gratis dan tanpa iklan.";

$getappnow = 'Dapatkan Sekarang';

$headline = 'Aplikasi Adzan hingga Qur’an dalam 1 Aplikasi. Gratis dan Tanpa Iklan. Aplikasi untuk membantu ibadah Anda. Adzan otomatis, jadwal sholat, al-Qur’an, hingga aqiqah dan qurban online. Semua hanya butuh 1 aplikasi saja!';
$judul  = 'Aplikasi Islami untuk Membantu Ibadah Anda';

$adzan  = "ADZAN";
$adzandesc = "Aplikasi adzan merupakan pemberitahuan visual dan audio untuk panggilan sholat dengan jadwal sholat yang akurat berdasarkan lokasi pengguna";

$quranjudul = "AL-QUR'AN";
$qurandesc  = 'Aplikasi Alquran lengkap 114 surah disertakan audio dan tajwid Juga terdapat terjemahan kementrian agama Indonesia serta penulisan latinnya. Penulisan arab Al-Quran disesuaikan dengan standard kementrian agama Indonesia.';

$jadwal_sholat      = "JADWAL SHOLAT";
$jadwal_sholatdesc  = "Aplikasi jadwal sholat yang akurat berdasarkan koordinat anda berada. Akan muncul suara adzan secara audio dan visual sebagai pengingat shalat.";

$kiblat = "ARAH KIBLAT";
$kiblatdesc = "Aplikasi arah kiblat merupakan kompas kiblat (penunjuk arah kiblat) yang membantu Anda mengetahui arah kiblat berdasarkan lokasi pengguna.";

$pencari_masjid_terdekat = "PENCARI MASJID TERDEKAT";
$pencari_masjid_terdekatdesc = "Fitur ini dibuat memudahkan kaum muslim menemukan (cari) masjid terdekat di lokasi yang tidak dikenal, di mana saja di seluruh dunia hingga memudahkan untuk sholat berjama'ah di awal waktu di masjid.";

$hadist = "HADIST PILIHAN";
$hadistdesc = "Hadist dijadikan sumber hukum Islam selain Alquran yang mana kedudukannya hadist merupakan sumber hukum kedua setelah Alquran. Muslimapp menyediakan kumpulan-kumpulan hadist untuk mempermudah pengguna dalam menkaji ilmu-ilmu Islam.";

$doa = "DOA PILIHAN";
$doadesc = "Terdapat kumpulan doa pilihan yang bisa dibaca dalam kegiatan sehari-hari. Mulai dari bangun tidur sampai hendak tidur. Juga terdapat doa untuk keluarga, doa kekuatan iman, serta doa-doa lainnya.";

$morefiturdesc = "Dan masih banyak fitur-fitur lainnya. Install muslimapp.id sekarang, Gratis, dan Tanpa Iklan!";

$Aqiqah = "AQIQAH";
$aqiqahh5 = "Fitur Layanan Aqiqah online berbasis mobile aplikasi pertama di Indonesia, keperluan Aqiqah kamu bakal lebih mudah, cepat, dan menyenangkan hanya dengan lewat aplikasi yang kami hadirkan untuk anda.";
$getnow = "Order Sekarang";

$Qurban = "QURBAN";
$qurbanh5 = "Fitur Layanan Qurban Online yang memudahkan kamu untuk menunaikan
ibadah kurban lebih praktis dan cepat.
";
$qurbangn = "Qurban Sekarang";

$hajjumroh = "HAJI & UMROH";
$hajjumro2 = "Haji & Umroh";
$donasi = "Donasi";
$hajjumrohh5 = "
Sempurnakan ibadah kamu dengan layanan paket Haji & Umrah dari kami, dengan memberikan jaminan fasilitas dan pelayanan yang lebih baik demi kekhusyuan Ibadah kamu.
";
$hajjbtn = "Pesan Sekarang";

$Wedding = "WEDDING";
$weddingh5 = "Fitur Layanan untuk keperluan pernikahan kamu dengan banyak pilihan paket
lengkap dan menarik, konsultasi dengan kami dan pilih sesuai dengan
kebutuhanmu.
";
$weddinggn = "Dapatkan Sekarang";

$islamic_product = "ISLAMIC PRODUCT";
$islamic_product_desc = "Pengalaman berbelanja berbagai produk muslim Beragam produk muslim ada disini Mulai Sekarang.";
$getnow_islamicproduct = "Mulai";

$feature = "FITUR KAMI";
$quran = "Al-Qur'an";
$ah = "Asma'ul Husna";
$aq = "Aqiqah";
$kal = "Kalender";
$qur = "Qurban";
$js = "Jadwal Shalat";
$k = "Kiblat";
$m = "Masjid";
$as = "Al -Matsurat";
$s = "Syahadat";
$d = "Do'a - Do'a";
$t = "Tasbih";
$kh = "Kumpulan Hadits";
$lm = "Live Mekkah";
$v = "Voucher";
$ip = "Produk Islami";
$gps = "GPS Lacak Haji Dan Umroh";

$tes1 = "Nice banget pokoknya. Aku minta dianter jam 10 dan on time banget. Makanannya juga enak. Praktis, mudah, no ribet.";
$tes2 = "Service atau pelayanan sangat baik, mulai dari informasi, konfirmasi  dilayani, dibantu  dengan baik, ramah.
        Paket aqiqah datang tepat waktu, kemasannya, bahannya dan desainnya bagus serta berkelas, olahan menu masakan baik dan rasanya tidak mengecewakan.
        Kami akan merekomendasikan layanan paket aqiqah dari muslimapp kepada yang lain.";
$tes3 = "Kemasan menarik, datang tepat waktu, pelayanan dari pemesanan hingga pengantaran sangat baik serta rasa masakannya enak. untuk masukan bisa ditambahkan sambal.";

$gt = "Dapatkan Sekarang!";

$foot1 = "Syarat dan Ketentuan";
$foot2 = "Kebijakan dan Privasi User";
$foot3 = "Hubungi Kami";

$cp = "Hak Cipta PT. Digital Muslim Global &copy; ".date("Y");

$con = "Kontak Whatsapp";

//Kebijakan privasi 
$line1 = 'Muslimapp.id merk dagang dari PT. Digital Muslim Global, Indonesia. menghormati privasi Anda dan berkomitmen untuk melindungi privasi pengguna aplikasi mobile sesuai dengan aturan perlindungan data yang berlaku, khususnya Regulasi Umum Perlindungan Data UE tanggal 27 April 2016 ("GDPR") serta hukum atau peraturan nasional apa pun yang menerapkan GDPR, dan / atau Undang-Undang Perlindungan Data Pribadi Indonesia Berdasarkan Undang-undang ITE (Informasi dan Transaksi Elektronik) beserta perubahannya (sekaligus, “Aturan yang Berlaku”), dan sebagaimana dijelaskan dalam Kebijakan Privasi ini (“Kebijakan”).';
$line2 = "Kebijakan Privasi adalah bentuk komitmen Muslimapp.id sebagai kepatuhan terhadap ketentuan perundang-undangan yang berlaku untuk melindungi kerahasiaan data atau informasi pribadi pengguna aplikasi Muslimapp.id.";
$line3 = "Kebijakan Privasi meliputi dasar atas perolehan, pengumpulan, pengolahan, penganalisaan, penampilan, pembukaan, dan/atau segala bentuk pengelolaan yang terkait dengan data atau informasi yang pengguna berikan kepada Muslimapp.id atau yang Muslimapp.id kumpulkan dari pengguna, termasuk data pribadi dari pengguna, baik pada saat pengguna melakukan pendaftaran pada aplikasi, mengakses aplikasi, maupun mempergunakan layanan-layanan pada aplikasi.";
$line4 = "Dengan mengakses dan/atau mempergunakan layanan Muslimapp.id, pengguna menyatakan bahwa setiap data pengguna merupakan data yang benar dan sah, serta pengguna memberikan persetujuan kepada Muslimapp.id untuk memperoleh, mengumpulkan, menyimpan, mengelola dan mempergunakan data tersebut sebagaimana termuat dalam kebijakan privasi maupun syarat dan ketentuan Muslimapp.id.";

?>
